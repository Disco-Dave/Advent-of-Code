module AdventOfCode.Solutions.Day03 (solve) where

import AdventOfCode.Answer (Answer (..), NotImplemented (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Parsers qualified as Parsers
import Control.Applicative (asum)
import Control.Monad (when)
import Control.Monad.ST qualified as ST
import Data.Attoparsec.Text qualified as Attoparsec
import Data.Char qualified as Char
import Data.Coerce (coerce)
import Data.Foldable (for_)
import Data.Functor (($>))
import Data.Maybe (mapMaybe)
import Data.STRef qualified as STRef
import Data.Text (Text)
import Data.Vector (Vector, (!?))
import Data.Vector qualified as Vector
import GHC.Natural (Natural)
import Text.Read (readMaybe)


data Tile
  = Empty
  | Number Natural
  | Symbol Char
  deriving (Show, Eq)


newtype EngineSchematic = EngineSchematic (Vector (Vector Tile))
  deriving (Show, Eq)


newtype PartNumber = PartNumber Natural
  deriving (Show, Eq)


newtype SumOfPartNumbers = SumOfPartNumbers Natural
  deriving (Show, Eq, Num) via Natural


parseInput :: Text -> Either ParseError EngineSchematic
parseInput =
  Parsers.parse $ do
    let
      tile =
        let
          empty = Attoparsec.char '.' $> Empty
          number = do
            char <- Attoparsec.anyChar
            case readMaybe [char] of
              Just n -> pure $ Number n
              Nothing -> fail $ "Expected a number but found: " <> [char]
          symbol = do
            char <- Attoparsec.anyChar

            if Char.isDigit char || char == '.' || char == '\n'
              then fail $ "Expected a symbol but found: " <> [char]
              else pure $ Symbol char
         in
          asum [empty, number, symbol]

      row =
        Vector.fromList <$> Attoparsec.many' tile

    rows <- row `Attoparsec.sepBy` Attoparsec.endOfLine

    pure . EngineSchematic $ Vector.fromList rows


type RowIndex = Int
type ColumnIndex = Int


lookupTile :: EngineSchematic -> RowIndex -> ColumnIndex -> Maybe Tile
lookupTile (EngineSchematic schematic) rowIndex columnIndex =
  (schematic !? rowIndex) >>= (!? columnIndex)


partNumbers :: EngineSchematic -> [PartNumber]
partNumbers (EngineSchematic schematic) =
  let
    isTouchingSymbol rowIndex columnIndex =
      let
        indexesToCheck =
          [ (rowIndex - 1, columnIndex - 1)
          , (rowIndex, columnIndex - 1)
          , (rowIndex - 1, columnIndex)
          , (rowIndex - 1, columnIndex + 1)
          , (rowIndex + 1, columnIndex - 1)
          , (rowIndex + 1, columnIndex + 1)
          , (rowIndex, columnIndex + 1)
          , (rowIndex + 1, columnIndex)
          ]

        tiles = mapMaybe (uncurry (lookupTile (EngineSchematic schematic))) indexesToCheck

        isSymbol = \case
          Symbol _ -> True
          _ -> False
       in
        any isSymbol tiles
   in
    zip [0 ..] (Vector.toList schematic) >>= \(rowIndex, row) ->
      ST.runST $ do
        activeDigitsRef <- STRef.newSTRef []
        touchedSymbolRef <- STRef.newSTRef False
        partNumbersRef <- STRef.newSTRef []

        let terminateNumber = do
              activeDigits <- STRef.readSTRef activeDigitsRef
              touchedSymbol <- STRef.readSTRef touchedSymbolRef

              when (not (null activeDigits) && touchedSymbol) $ do
                let partNumber =
                      PartNumber . sum $
                        [ digit * (10 ^ multiplier)
                        | (multiplier, digit) <- zip [0 :: Integer ..] activeDigits
                        ]

                STRef.modifySTRef' partNumbersRef $! (partNumber :)

              STRef.writeSTRef activeDigitsRef []
              STRef.writeSTRef touchedSymbolRef False

        for_ (zip [0 ..] (Vector.toList row)) $ \(columnIndex, tile) ->
          case tile of
            Number digit -> do
              STRef.modifySTRef' activeDigitsRef $! (digit :)

              let touchingSymbol = isTouchingSymbol rowIndex columnIndex

              when touchingSymbol $
                STRef.writeSTRef touchedSymbolRef True
            _ ->
              terminateNumber

        terminateNumber

        STRef.readSTRef partNumbersRef


partOne :: EngineSchematic -> SumOfPartNumbers
partOne =
  SumOfPartNumbers . sum . coerce @_ @[Natural] . partNumbers


solve :: Text -> Either ParseError (Answer SumOfPartNumbers NotImplemented)
solve input = do
  schematic <- parseInput input

  Right $
    Answer
      { partOne = partOne schematic
      , partTwo = NotImplemented
      }
