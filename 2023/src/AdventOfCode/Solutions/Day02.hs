module AdventOfCode.Solutions.Day02 (solve) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Parsers qualified as Parsers
import Control.Applicative (asum)
import Data.Attoparsec.Text qualified as Attoparsec
import Data.Coerce (coerce)
import Data.Foldable (foldl')
import Data.Functor (($>))
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Text (Text)
import GHC.Natural (Natural)


data CubeColor
  = Red
  | Green
  | Blue
  deriving (Show, Eq, Ord)


type CubeCount = Natural


type CubeCounts = Map CubeColor CubeCount


newtype GameId = GameId Integer
  deriving (Show, Eq, Ord)


data Game = Game
  { id :: GameId
  , turns :: [CubeCounts]
  }
  deriving (Show, Eq)


parseInput :: Text -> Either ParseError [Game]
parseInput =
  Parsers.parse $ do
    let
      cubeColor =
        asum
          [ Attoparsec.string "blue" $> Blue
          , Attoparsec.string "red" $> Red
          , Attoparsec.string "green" $> Green
          ]

      countAndCube = do
        count <- Parsers.natural
        _ <- Attoparsec.space
        color <- cubeColor
        pure (color, count)

      gameTurn = do
        turns <- countAndCube `Attoparsec.sepBy` Attoparsec.string ", "
        pure $ Map.fromListWith (+) turns

      game = do
        _ <- Attoparsec.string "Game "
        gameId <- fmap GameId Parsers.integer
        _ <- Attoparsec.string ": "
        turns <- gameTurn `Attoparsec.sepBy` Attoparsec.string "; "
        pure Game{id = gameId, turns}

    game `Attoparsec.sepBy` Attoparsec.endOfLine


partOne :: (CubeColor -> CubeCount) -> [Game] -> Integer
partOne maxCounts games =
  let
    isValidTurn turn =
      flip all (Map.toList turn) $ \(color, count) ->
        count <= maxCounts color

    isValidGame Game{turns} =
      all isValidTurn turns

    processGame runningTotal game
      | isValidGame game = runningTotal + coerce game.id
      | otherwise = runningTotal
   in
    foldl' processGame 0 games


partTwo :: [Game] -> Integer
partTwo games =
  let
    calculatePower Game{turns} =
      product . fmap toInteger $ Map.elems (Map.unionsWith max turns)
   in
    sum $ fmap calculatePower games


solve :: Text -> Either ParseError (Answer Integer Integer)
solve raw = do
  games <- parseInput raw

  let
    maxCounts = \case
      Red -> 12
      Green -> 13
      Blue -> 14

  Right $
    Answer
      { partOne = partOne maxCounts games
      , partTwo = partTwo games
      }
