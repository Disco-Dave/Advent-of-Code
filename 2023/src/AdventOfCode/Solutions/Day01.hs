module AdventOfCode.Solutions.Day01
  ( solve
  ) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import Data.Char qualified as Char
import Data.Coerce (coerce)
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (mapMaybe)
import Data.Text (Text)
import Data.Text qualified as Text
import Text.Read (readMaybe)


newtype ArtisticCalibrationValue = ArtisticCalibrationValue Text
  deriving (Show, Eq)


newtype CalibrationDocument = CalibrationDocument [ArtisticCalibrationValue]
  deriving (Show, Eq)


newtype InterpretDigits = InterpretDigits (ArtisticCalibrationValue -> [Integer])


newtype SumOfCalibrationValues = SumOfCalibrationValues Integer
  deriving (Show, Eq, Ord, Num) via Integer


parseInput :: Text -> CalibrationDocument
parseInput raw =
  CalibrationDocument . fmap ArtisticCalibrationValue $ Text.lines raw


sumValues :: InterpretDigits -> CalibrationDocument -> SumOfCalibrationValues
sumValues (InterpretDigits interpret) (CalibrationDocument document) =
  let
    calculateCalibrationValue line =
      case NonEmpty.nonEmpty (interpret line) of
        Nothing -> 0
        Just digits ->
          (NonEmpty.head digits * 10) + NonEmpty.last digits
   in
    SumOfCalibrationValues . sum $ fmap calculateCalibrationValue document


parseDigits :: Text -> [Integer]
parseDigits =
  mapMaybe (readMaybe @Integer . pure @[]) . Text.unpack


parseWords :: Text -> [Integer]
parseWords raw =
  let
    normalizedText = Text.toLower $ Text.strip raw

    parsePrefix text
      | Text.isPrefixOf "zero" text = Just 0
      | Text.isPrefixOf "one" text = Just 1
      | Text.isPrefixOf "two" text = Just 2
      | Text.isPrefixOf "three" text = Just 3
      | Text.isPrefixOf "four" text = Just 4
      | Text.isPrefixOf "five" text = Just 5
      | Text.isPrefixOf "six" text = Just 6
      | Text.isPrefixOf "seven" text = Just 7
      | Text.isPrefixOf "eight" text = Just 8
      | Text.isPrefixOf "nine" text = Just 9
      | otherwise = Nothing

    go curr digits
      | Text.null curr = reverse digits
      | otherwise =
          go (Text.drop 1 curr) $
            case parsePrefix curr of
              Just digit -> digit : digits
              Nothing -> digits
   in
    go normalizedText []


partOne :: CalibrationDocument -> SumOfCalibrationValues
partOne =
  sumValues . InterpretDigits $ (parseDigits . coerce)


partTwo :: CalibrationDocument -> SumOfCalibrationValues
partTwo =
  sumValues . InterpretDigits $ \(ArtisticCalibrationValue raw) ->
    let
      brokenUpByDigitsOrLetters =
        Text.groupBy (\a b -> Char.isDigit a == Char.isDigit b) raw

      parseGroup text
        | Text.all Char.isDigit text = parseDigits text
        | otherwise = parseWords text
     in
      parseGroup =<< brokenUpByDigitsOrLetters


solve :: Text -> Either ParseError (Answer SumOfCalibrationValues SumOfCalibrationValues)
solve input = do
  let calibrationDocument = parseInput input

  Right $
    Answer
      { partOne = partOne calibrationDocument
      , partTwo = partTwo calibrationDocument
      }
