module AdventOfCode.Solutions.Day01Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day01 qualified as Day01
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, shouldBe)


spec :: Spec
spec = do
  describe "part one" $ do
    it "works with example input" $ do
      Right answer <-
        pure . Day01.solve $
          Text.unlines
            [ "1abc2"
            , "pqr3stu8vwx"
            , "a1b2c3d4e5f"
            , "treb7uchet"
            ]

      answer.partOne `shouldBe` 142

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day01
      Right answer <- pure $ Day01.solve puzzleInput
      answer.partOne `shouldBe` 53974

  describe "part two" $ do
    it "works with example input" $ do
      Right answer <-
        pure . Day01.solve $
          Text.unlines
            [ "two1nine"
            , "eightwothree"
            , "abcone2threexyz"
            , "xtwone3four"
            , "4nineeightseven2"
            , "zoneight234"
            , "7pqrstsixteen"
            ]

      answer.partTwo `shouldBe` 281

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day01
      Right answer <- pure $ Day01.solve puzzleInput
      answer.partTwo `shouldBe` 52840
