module AdventOfCode.Solutions.Day02Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day02 qualified as Day02
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, shouldBe)


spec :: Spec
spec = do
  describe "part one" $ do
    it "works with example input" $ do
      let
        example =
          Text.unlines
            [ "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"
            , "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue"
            , "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red"
            , "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red"
            , "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"
            ]
      Right answer <- pure $ Day02.solve example
      answer.partOne `shouldBe` 8

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day02
      Right answer <- pure $ Day02.solve puzzleInput
      answer.partOne `shouldBe` 2156

  describe "part two" $ do
    it "works with example input" $ do
      let
        example =
          Text.unlines
            [ "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"
            , "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue"
            , "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red"
            , "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red"
            , "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"
            ]
      Right answer <- pure $ Day02.solve example
      answer.partTwo `shouldBe` 2286

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day02
      Right answer <- pure $ Day02.solve puzzleInput
      answer.partTwo `shouldBe` 66909
