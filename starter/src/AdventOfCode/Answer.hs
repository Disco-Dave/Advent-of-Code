module AdventOfCode.Answer
  ( NotImplemented (..)
  , Answer (..)
  , prettyPrint
  )
where

import Control.DeepSeq qualified as DeepSeq
import Data.Text (Text)
import Data.Text qualified as Text


data NotImplemented = NotImplemented
  deriving (Eq)


instance Show NotImplemented where
  show _ =
    "Not implemented"


instance DeepSeq.NFData NotImplemented where
  rnf _ =
    ()


data Answer partOne partTwo = Answer
  { partOne :: partOne
  , partTwo :: partTwo
  }
  deriving (Show, Eq)


instance DeepSeq.NFData partOne => DeepSeq.NFData1 (Answer partOne) where
  liftRnf forcePartTwo Answer{partTwo} =
    forcePartTwo partTwo


instance DeepSeq.NFData2 Answer where
  liftRnf2 forcePart1 forcePart2 Answer{partOne, partTwo} =
    forcePart1 partOne `seq` forcePart2 partTwo


instance (DeepSeq.NFData partOne, DeepSeq.NFData partTwo) => DeepSeq.NFData (Answer partOne partTwo) where
  rnf =
    DeepSeq.rnf2


prettyPrint :: (Show partOne, Show partTwo) => Answer partOne partTwo -> Text
prettyPrint Answer{partOne, partTwo} =
  let partOneText = "Part 1: " <> Text.pack (show partOne)
      partTwoText = "Part 2: " <> Text.pack (show partTwo)
   in partOneText <> "\n" <> partTwoText
