module AdventOfCode.Solutions.Day13Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Answer qualified as Answer
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day13 qualified as Day13
import Test.Hspec (Spec, describe, it, pending, shouldBe)


spec :: Spec
spec = do
  describe "part one" $ do
    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day13
      Right answer <- pure $ Day13.solve puzzleInput
      answer.partOne `shouldBe` Answer.NotImplemented
      pending

  describe "part two" $ do
    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day13
      Right answer <- pure $ Day13.solve puzzleInput
      answer.partTwo `shouldBe` Answer.NotImplemented
      pending
