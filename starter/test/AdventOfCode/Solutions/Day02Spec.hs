module AdventOfCode.Solutions.Day02Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Answer qualified as Answer
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day02 qualified as Day02
import Test.Hspec (Spec, describe, it, pending, shouldBe)


spec :: Spec
spec = do
  describe "part one" $ do
    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day02
      Right answer <- pure $ Day02.solve puzzleInput
      answer.partOne `shouldBe` Answer.NotImplemented
      pending

  describe "part two" $ do
    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day02
      Right answer <- pure $ Day02.solve puzzleInput
      answer.partTwo `shouldBe` Answer.NotImplemented
      pending
