module AdventOfCode (run) where

import AdventOfCode.Answer qualified as Answer
import AdventOfCode.Cli qualified as Cli
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Parsers qualified as Parsers
import AdventOfCode.Solutions (PuzzlesPath)
import AdventOfCode.Solutions qualified as Solutions
import Control.Concurrent.Async qualified as Async
import Control.Concurrent.STM qualified as STM
import Control.DeepSeq qualified as DeepSeq
import Control.Monad (unless, when)
import Data.Foldable (for_)
import Data.List.NonEmpty qualified as NonEmpty
import Data.Set qualified as Set
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Text.IO qualified as Text.IO
import System.Clock qualified as Clock


executeSolution :: PuzzlesPath -> Solutions.Day -> IO Text
executeSolution puzzlesPath day = do
  puzzleInput <- Solutions.getInput puzzlesPath day

  start <- Clock.getTime Clock.Monotonic
  let !result = DeepSeq.force $ Solutions.solve puzzleInput day
  end <- Clock.getTime Clock.Monotonic

  let header =
        let ellapsedTime = end - start
            ellpasedTimeInMilliseconds =
              fromInteger @Double (Clock.toNanoSecs ellapsedTime) / 1_000_000
            dayText =
              Text.pack $ "Day " <> show (fromEnum day + 1)
         in dayText <> "\t(" <> Text.pack (show ellpasedTimeInMilliseconds) <> " ms)"

      prettyAnswer =
        case result of
          Right answer ->
            let partOneText = "Part 1: " <> answer.partOne
                partTwoText = "Part 2: " <> answer.partTwo
             in partOneText <> "\n" <> partTwoText
          Left err ->
            Parsers.prettyPrintError err

  pure $ header <> "\n" <> prettyAnswer


data Event
  = SolutionReceived Text
  | Finished


parallelRun :: PuzzlesPath -> [Solutions.Day] -> IO ()
parallelRun puzzlesPath days = do
  queue <- STM.newTQueueIO
  shouldPrintNewlineVar <- STM.newTVarIO False

  let printHandler = do
        (progress, shouldPrintNewline) <-
          STM.atomically $ do
            progress <- STM.readTQueue queue
            shouldPrintNewline <- STM.readTVar shouldPrintNewlineVar

            unless shouldPrintNewline $
              STM.writeTVar shouldPrintNewlineVar True

            pure (progress, shouldPrintNewline)

        case progress of
          Finished -> pure ()
          SolutionReceived solution -> do
            when shouldPrintNewline $
              Text.IO.putStrLn ""

            Text.IO.putStrLn solution

            printHandler

      runSolution day = do
        solution <- executeSolution puzzlesPath day
        STM.atomically . STM.writeTQueue queue $ SolutionReceived solution

  Async.withAsync printHandler $ \printHandlerTask -> do
    Async.mapConcurrently_ runSolution days
    STM.atomically $ STM.writeTQueue queue Finished
    Async.wait printHandlerTask


sequentialRun :: PuzzlesPath -> [Solutions.Day] -> IO ()
sequentialRun puzzlesPath days =
  for_ (zip [(0 :: Int) ..] days) $ \(index, day) -> do
    when (index /= 0) $
      Text.IO.putStrLn ""

    Text.IO.putStrLn =<< executeSolution puzzlesPath day


run :: IO ()
run = do
  envVars <- EnvVars.parse
  args <- Cli.getArgs

  let runner =
        case args.mode of
          Cli.Parallel -> parallelRun
          Cli.Sequential -> sequentialRun

      days =
        Set.toAscList . Set.fromList $ NonEmpty.toList args.days

  runner envVars.puzzlesPath days
