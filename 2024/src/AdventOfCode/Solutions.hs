module AdventOfCode.Solutions
  ( Day (..)
  , PuzzleInput (..)
  , PuzzlesPath (..)
  , getInput
  , solve
  )
where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Solutions.Day01 qualified as Day01
import AdventOfCode.Solutions.Day02 qualified as Day02
import AdventOfCode.Solutions.Day03 qualified as Day03
import AdventOfCode.Solutions.Day04 qualified as Day04
import AdventOfCode.Solutions.Day05 qualified as Day05
import AdventOfCode.Solutions.Day06 qualified as Day06
import AdventOfCode.Solutions.Day07 qualified as Day07
import AdventOfCode.Solutions.Day08 qualified as Day08
import AdventOfCode.Solutions.Day09 qualified as Day09
import AdventOfCode.Solutions.Day10 qualified as Day10
import AdventOfCode.Solutions.Day11 qualified as Day11
import AdventOfCode.Solutions.Day12 qualified as Day12
import AdventOfCode.Solutions.Day13 qualified as Day13
import AdventOfCode.Solutions.Day14 qualified as Day14
import AdventOfCode.Solutions.Day15 qualified as Day15
import AdventOfCode.Solutions.Day16 qualified as Day16
import AdventOfCode.Solutions.Day17 qualified as Day17
import AdventOfCode.Solutions.Day18 qualified as Day18
import AdventOfCode.Solutions.Day19 qualified as Day19
import AdventOfCode.Solutions.Day20 qualified as Day20
import AdventOfCode.Solutions.Day21 qualified as Day21
import AdventOfCode.Solutions.Day22 qualified as Day22
import AdventOfCode.Solutions.Day23 qualified as Day23
import AdventOfCode.Solutions.Day24 qualified as Day24
import AdventOfCode.Solutions.Day25 qualified as Day25
import Data.ByteString qualified as ByteString
import Data.Functor ((<&>))
import Data.String (IsString)
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Text.Encoding (decodeUtf8)
import System.FilePath ((</>))


data Day
  = Day01
  | Day02
  | Day03
  | Day04
  | Day05
  | Day06
  | Day07
  | Day08
  | Day09
  | Day10
  | Day11
  | Day12
  | Day13
  | Day14
  | Day15
  | Day16
  | Day17
  | Day18
  | Day19
  | Day20
  | Day21
  | Day22
  | Day23
  | Day24
  | Day25
  deriving (Show, Eq, Ord, Enum, Bounded)


newtype PuzzlesPath = PuzzlesPath FilePath
  deriving (Show, Eq, Ord, IsString)


newtype PuzzleInput = PuzzleInput Text
  deriving (Show, Eq, Ord, IsString)


getInput :: PuzzlesPath -> Day -> IO PuzzleInput
getInput (PuzzlesPath puzzlesPath) day =
  fmap (PuzzleInput . decodeUtf8) . ByteString.readFile $
    puzzlesPath </> (show day <> ".txt")


solve :: PuzzleInput -> Day -> Either ParseError (Answer Text Text)
solve (PuzzleInput input) day =
  let execute runDay =
        runDay input <&> \Answer{partOne, partTwo} ->
          Answer
            { partOne = Text.pack $ show partOne
            , partTwo = Text.pack $ show partTwo
            }
   in case day of
        Day01 -> execute Day01.solve
        Day02 -> execute Day02.solve
        Day03 -> execute Day03.solve
        Day04 -> execute Day04.solve
        Day05 -> execute Day05.solve
        Day06 -> execute Day06.solve
        Day07 -> execute Day07.solve
        Day08 -> execute Day08.solve
        Day09 -> execute Day09.solve
        Day10 -> execute Day10.solve
        Day11 -> execute Day11.solve
        Day12 -> execute Day12.solve
        Day13 -> execute Day13.solve
        Day14 -> execute Day14.solve
        Day15 -> execute Day15.solve
        Day16 -> execute Day16.solve
        Day17 -> execute Day17.solve
        Day18 -> execute Day18.solve
        Day19 -> execute Day19.solve
        Day20 -> execute Day20.solve
        Day21 -> execute Day21.solve
        Day22 -> execute Day22.solve
        Day23 -> execute Day23.solve
        Day24 -> execute Day24.solve
        Day25 -> execute Day25.solve
