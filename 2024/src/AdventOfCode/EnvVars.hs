module AdventOfCode.EnvVars
  ( EnvVars (..)
  , parse
  )
where

import AdventOfCode.Solutions (PuzzlesPath (PuzzlesPath))
import Control.Monad ((<=<))
import Data.Maybe (fromMaybe)
import Env qualified
import Paths_advent_of_code qualified


newtype EnvVars = EnvVars
  { puzzlesPath :: PuzzlesPath
  }
  deriving (Show, Eq)


parse :: IO EnvVars
parse = do
  maybePuzzlesPath <- do
    Env.parse (Env.header "advent of code") $
      Env.optional (Env.var (Env.str <=< Env.nonempty) "ADVENT_OF_CODE_PUZZLES_PATH" (Env.help "Path to the puzzle inputs"))

  defaultPuzzlesPath <- Paths_advent_of_code.getDataFileName "puzzles"

  pure $
    EnvVars
      { puzzlesPath = fromMaybe (PuzzlesPath defaultPuzzlesPath) maybePuzzlesPath
      }
