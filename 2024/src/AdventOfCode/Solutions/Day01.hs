module AdventOfCode.Solutions.Day01
  ( Distance (..)
  , SimilarityScore (..)
  , solve
  ) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Parsers qualified as Parsers

import Data.Attoparsec.Text qualified as Attoparsec
import Data.Function (on)
import Data.List (sort)
import Data.Map.Strict qualified as Map
import Data.Maybe (fromMaybe)
import Data.Text (Text)


newtype LocationId = LocationId Integer
  deriving (Show, Eq, Ord, Enum, Num, Real, Integral)


data Row = Row
  { left :: LocationId
  , right :: LocationId
  }
  deriving (Show, Eq, Ord)


newtype Distance = Distance Integer
  deriving (Eq, Ord, Enum, Num, Real, Integral)
  deriving (Show) via Integer


newtype SimilarityScore = SimilarityScore Integer
  deriving (Eq, Ord, Enum, Num, Real, Integral)
  deriving (Show) via Integer


parseInput :: Text -> Either ParseError [Row]
parseInput =
  Parsers.parse $ do
    let
      parseLocationId =
        fmap LocationId Parsers.integer

      parseRow = do
        left <- parseLocationId
        Attoparsec.skipSpace
        Row left <$> parseLocationId

    parseRow `Attoparsec.sepBy` Attoparsec.endOfLine


splitLocationList :: [Row] -> ([LocationId], [LocationId])
splitLocationList =
  unzip . fmap (\r -> (r.left, r.right))


solvePartOne :: [LocationId] -> [LocationId] -> Distance
solvePartOne leftList rightList =
  let
    sortedLocations =
      on (zipWith Row) sort leftList rightList

    computeDistance row =
      Distance . toInteger $ abs (row.left - row.right)

    distances =
      fmap computeDistance sortedLocations
   in
    sum distances


solvePartTwo :: [LocationId] -> [LocationId] -> SimilarityScore
solvePartTwo leftList rightList =
  let
    rightListFrequencyMap =
      Map.fromListWithKey (\_ _ count -> count + 1) (fmap (,1) rightList)

    computeSimilaryScore locationId =
      let
        numberOfOccurances =
          fromMaybe 0 $ Map.lookup locationId rightListFrequencyMap
       in
        SimilarityScore $ toInteger locationId * numberOfOccurances

    scores =
      fmap computeSimilaryScore leftList
   in
    sum scores


solve :: Text -> Either ParseError (Answer Distance SimilarityScore)
solve rawInput = do
  (leftList, rightList) <- splitLocationList <$> parseInput rawInput
  pure $
    Answer
      { partOne = solvePartOne leftList rightList
      , partTwo = solvePartTwo leftList rightList
      }
