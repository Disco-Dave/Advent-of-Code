module AdventOfCode.Solutions.Day02
  ( TotalSafeReports (..)
  , solve
  ) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Parsers qualified as Parsers

import Data.Attoparsec.Text qualified as Attoparsec
import Data.List.NonEmpty (NonEmpty)
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (mapMaybe)
import Data.Text (Text)


newtype Level = Level Integer
  deriving (Show, Eq, Ord, Enum, Num, Real, Integral)


newtype Report = Report (NonEmpty Level)
  deriving (Show, Eq, Ord)


newtype TotalSafeReports = TotalSafeReports Integer
  deriving (Eq, Ord, Enum, Num, Real, Integral)
  deriving (Show) via Integer


data LevelTrend
  = Decreasing
  | Increasing
  deriving (Show, Eq, Ord, Enum, Bounded)


data TheProblemDampener
  = Disabled
  | Enabled
  deriving (Show, Eq, Ord, Enum, Bounded)


parseInput :: Text -> Either ParseError [Report]
parseInput =
  Parsers.parse $ do
    let
      parseLevel =
        fmap Level Parsers.integer

      parseReport = do
        levels <- parseLevel `Attoparsec.sepBy` Attoparsec.char ' '
        maybe (fail "Report must have at least one level") (pure . Report) $
          NonEmpty.nonEmpty levels

    parseReport `Attoparsec.sepBy` Attoparsec.endOfLine


isReportSafe :: TheProblemDampener -> Report -> Bool
isReportSafe dampener report =
  let
    reports =
      case dampener of
        Disabled -> [report]
        Enabled ->
          let
            Report levels = report

            indexes = [0 .. NonEmpty.length levels - 1]

            levelsWithIndex = zip indexes (NonEmpty.toList levels)

            buildReport indexToDrop =
              let results = mapMaybe (\(i, l) -> if i == indexToDrop then Nothing else Just l) levelsWithIndex
               in Report <$> NonEmpty.nonEmpty results
           in
            report : mapMaybe buildReport indexes
   in
    flip any reports $ \(Report levels) ->
      let
        firstLevel = NonEmpty.head levels
       in
        case NonEmpty.tail levels of
          [] -> True
          (secondLevel : otherLevels) ->
            let
              levelTrend
                | firstLevel < secondLevel = Increasing
                | otherwise = Decreasing

              isAcceptable (Level prev, Level next) =
                let
                  difference =
                    abs (prev - next)

                  isTrendingInTheRightDirection =
                    case levelTrend of
                      Increasing -> prev < next
                      Decreasing -> prev > next

                  isWithinRange =
                    difference >= 1
                      && difference <= 3
                 in
                  isTrendingInTheRightDirection && isWithinRange

              levelPairs =
                scanl
                  (\(_, prev) next -> (prev, next))
                  (firstLevel, secondLevel)
                  otherLevels
             in
              all isAcceptable levelPairs


countSafeReports :: TheProblemDampener -> [Report] -> TotalSafeReports
countSafeReports dampener reports =
  TotalSafeReports . toInteger . length $
    filter (isReportSafe dampener) reports


solvePartOne :: [Report] -> TotalSafeReports
solvePartOne =
  countSafeReports Disabled


solvePartTwo :: [Report] -> TotalSafeReports
solvePartTwo =
  countSafeReports Enabled


solve :: Text -> Either ParseError (Answer TotalSafeReports TotalSafeReports)
solve rawInput = do
  reports <- parseInput rawInput

  Right $
    Answer
      { partOne = solvePartOne reports
      , partTwo = solvePartTwo reports
      }
