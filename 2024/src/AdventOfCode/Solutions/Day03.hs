module AdventOfCode.Solutions.Day03 (solve) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Parsers qualified as Parsers

import Data.Attoparsec.Text qualified as Attoparsec
import Data.Functor (($>))
import Data.List (foldl')
import Data.Maybe (catMaybes)
import Data.Text (Text)


data Instruction
  = Multiply Integer Integer
  | Do
  | Don't
  deriving (Show, Eq, Ord)


type Program = [Instruction]


parseInput :: Text -> Either ParseError Program
parseInput =
  Parsers.parse $ do
    let
      parseMultiply = do
        _ <- Attoparsec.string "mul("
        a <- Parsers.integer
        _ <- Attoparsec.char ','
        b <- Parsers.integer
        _ <- Attoparsec.char ')'
        pure $ Multiply a b

      parseInstruction =
        Attoparsec.choice
          [ parseMultiply
          , Attoparsec.string "do()" $> Do
          , Attoparsec.string "don't()" $> Don't
          ]

      parseFuzzyInstruction =
        Attoparsec.choice
          [ fmap Just parseInstruction
          , Attoparsec.anyChar $> Nothing
          ]

    catMaybes <$> Attoparsec.many1 parseFuzzyInstruction


interpret :: state -> (state -> Instruction -> state) -> Program -> state
interpret initial runInstruction =
  foldl' runInstruction initial


solvePartOne :: Program -> Integer
solvePartOne =
  interpret 0 $ \(!total) -> \case
    Multiply a b -> a * b + total
    _ -> total


solvePartTwo :: Program -> Integer
solvePartTwo =
  fmap fst . interpret (0, True) $ \(!total, !isEnabled) -> \case
    Multiply a b
      | isEnabled -> (a * b + total, isEnabled)
      | otherwise -> (total, isEnabled)
    Do -> (total, True)
    Don't -> (total, False)


solve :: Text -> Either ParseError (Answer Integer Integer)
solve rawInput = do
  program <- parseInput rawInput

  Right $
    Answer
      { partOne = solvePartOne program
      , partTwo = solvePartTwo program
      }
