module AdventOfCode.Solutions.Day06 (solve) where

import AdventOfCode.Answer (Answer (..), NotImplemented (..))
import AdventOfCode.Parsers (ParseError)
import Data.Text (Text)


solve :: Text -> Either ParseError (Answer NotImplemented NotImplemented)
solve _ =
  Right $
    Answer
      { partOne = NotImplemented
      , partTwo = NotImplemented
      }
