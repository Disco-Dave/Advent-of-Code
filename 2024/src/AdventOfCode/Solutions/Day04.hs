module AdventOfCode.Solutions.Day04
  ( WordCount (..)
  , XmasCount
  , solve
  ) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)

import AdventOfCode.Parsers qualified as Parsers
import Data.Attoparsec.Text qualified as Attoparsec
import Data.Maybe (mapMaybe)
import Data.Set (Set)
import Data.Set qualified as Set
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Vector.Strict (Vector)
import Data.Vector.Strict qualified as Vector
import GHC.TypeLits (Symbol)


newtype WordSearchPuzzle = WordSearchPuzzle (Vector (Vector Char))
  deriving (Show, Eq)


newtype Row = Row Int
  deriving (Eq, Ord, Enum, Bounded, Num, Real, Integral)
  deriving (Show) via Int


newtype Column = Column Int
  deriving (Eq, Ord, Enum, Bounded, Num, Real, Integral)
  deriving (Show) via Int


type Point = (Column, Row)


data Dimensions = Dimensions
  { width :: Int
  , height :: Int
  }
  deriving (Show, Eq)


newtype WordCount (word :: Symbol) = WordCount Integer
  deriving (Eq, Ord, Enum, Num, Real, Integral)
  deriving (Show) via Integer


type XmasCount = WordCount "XMAS"


lookupDimensions :: WordSearchPuzzle -> Dimensions
lookupDimensions (WordSearchPuzzle columns) =
  let
    totalColumns = Vector.length columns

    totalRows = maybe 0 Vector.length $ columns Vector.!? 0
   in
    Dimensions
      { width = totalRows
      , height = totalColumns
      }


lookupPoint :: WordSearchPuzzle -> Point -> Maybe Char
lookupPoint (WordSearchPuzzle vector) (Column column, Row row) =
  vector Vector.!? row
    >>= (Vector.!? column)


allPoints :: WordSearchPuzzle -> [Point]
allPoints puzzle =
  let
    Dimensions{width, height} = lookupDimensions puzzle
   in
    [0 .. height - 1] >>= \c ->
      [(Column c, Row r) | r <- [0 .. width - 1]]


parseInput :: Text -> Either ParseError WordSearchPuzzle
parseInput =
  Parsers.parse $ do
    let
      parsePoint =
        Attoparsec.choice $ fmap Attoparsec.char ('.' : ['A' .. 'Z'])

      parseRow =
        Vector.fromList <$> Attoparsec.many1 parsePoint

    rows <- parseRow `Attoparsec.sepBy` Attoparsec.endOfLine

    pure . WordSearchPuzzle $ Vector.fromList rows


findWordNear :: WordSearchPuzzle -> Text -> Point -> Set (Set Point)
findWordNear puzzle word (column, row) =
  let
    wordLength = Text.length word

    horizontalPointsToCheck =
      [ fmap (,row) [column .. column + fromIntegral wordLength - 1]
      , fmap (,row) [column, column - 1 .. column - fromIntegral wordLength + 1]
      ]

    verticalPointsToCheck =
      [ fmap (column,) [row .. row + fromIntegral wordLength - 1]
      , fmap (column,) [row, row - 1 .. fromIntegral wordLength - 1]
      ]

    forwardslashPointsToCheck =
      [ zip
          [column, column - 1 .. column - fromIntegral wordLength + 1]
          [row .. row + fromIntegral wordLength - 1]
      , zip
          [column, column - 1 .. column - fromIntegral wordLength + 1]
          [row, row - 1 .. fromIntegral wordLength - 1]
      ]

    backslashPointsToCheck =
      [ zip
          [column .. column + fromIntegral wordLength - 1]
          [row .. row + fromIntegral wordLength - 1]
      , zip
          [column .. column + fromIntegral wordLength - 1]
          [row, row - 1 .. fromIntegral wordLength - 1]
      ]

    pointsToCheck =
      mconcat
        [ horizontalPointsToCheck
        , verticalPointsToCheck
        , forwardslashPointsToCheck
        , backslashPointsToCheck
        ]

    matchesWord points =
      let
        actual = Text.pack $ mapMaybe (lookupPoint puzzle) points
       in
        actual == word || Text.reverse actual == word
   in
    Set.fromList . fmap Set.fromList $
      filter matchesWord pointsToCheck


solvePartOne :: WordSearchPuzzle -> XmasCount
solvePartOne puzzle =
  let
    pointsWithXmas =
      foldMap (findWordNear puzzle "XMAS") (allPoints puzzle)
   in
    fromIntegral $ Set.size pointsWithXmas


solvePartTwo :: WordSearchPuzzle -> XmasCount
solvePartTwo puzzle =
  let
    findCross (column, row) =
      let
        forwardslashPointsToCheck =
          [(column - 1, row + 1), (column, row), (column + 1, row - 1)]

        backslashPointsToCheck =
          [(column - 1, row - 1), (column, row), (column + 1, row + 1)]

        forwardslash =
          Text.pack $
            mapMaybe
              (lookupPoint puzzle)
              forwardslashPointsToCheck

        backwardslash =
          Text.pack $
            mapMaybe
              (lookupPoint puzzle)
              backslashPointsToCheck

        forwardMatches =
          forwardslash == "MAS"
            || forwardslash == "SAM"

        backwardMatches =
          backwardslash == "MAS"
            || backwardslash == "SAM"
       in
        if forwardMatches && backwardMatches
          then Set.singleton (Set.fromList (forwardslashPointsToCheck <> backslashPointsToCheck))
          else Set.empty

    pointsWithXmas =
      foldMap findCross (allPoints puzzle)
   in
    fromIntegral $ Set.size pointsWithXmas


solve :: Text -> Either ParseError (Answer XmasCount XmasCount)
solve rawInput = do
  puzzle <- parseInput rawInput

  Right $
    Answer
      { partOne = solvePartOne puzzle
      , partTwo = solvePartTwo puzzle
      }
