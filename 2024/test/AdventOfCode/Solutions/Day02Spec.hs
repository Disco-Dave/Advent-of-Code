module AdventOfCode.Solutions.Day02Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day02 qualified as Day02

import Data.Text (Text)
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, runIO, shouldBe)


exampleInput :: Text
exampleInput =
  Text.unlines
    [ "7 6 4 2 1"
    , "1 2 7 8 9"
    , "9 7 6 2 1"
    , "1 3 2 4 5"
    , "8 6 4 4 1"
    , "1 3 6 7 9"
    ]


spec :: Spec
spec = do
  (PuzzleInput puzzleInput) <- runIO $ do
    vars <- EnvVars.parse
    Solutions.getInput vars.puzzlesPath Solutions.Day02

  let
    exampleAnswers = Day02.solve exampleInput
    puzzleAnswers = Day02.solve puzzleInput

  describe "part one" $ do
    it "works with example input" $ do
      fmap (.partOne) exampleAnswers `shouldBe` Right 2

    it "works with puzzle input" $ do
      fmap (.partOne) puzzleAnswers `shouldBe` Right 269

  describe "part two" $ do
    it "works with example input" $ do
      fmap (.partTwo) exampleAnswers `shouldBe` Right 4

    it "works with puzzle input" $ do
      fmap (.partTwo) puzzleAnswers `shouldBe` Right 337
