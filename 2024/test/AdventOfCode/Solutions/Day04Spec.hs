module AdventOfCode.Solutions.Day04Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day04 qualified as Day04

import Data.Text (Text)
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, runIO, shouldBe)


exampleInput :: Text
exampleInput =
  Text.unlines
    [ "MMMSXXMASM"
    , "MSAMXMSMSA"
    , "AMXSXMAAMM"
    , "MSAMASMSMX"
    , "XMASAMXAMM"
    , "XXAMMXXAMA"
    , "SMSMSASXSS"
    , "SAXAMASAAA"
    , "MAMMMXMMMM"
    , "MXMXAXMASX"
    ]


spec :: Spec
spec = do
  (PuzzleInput puzzleInput) <- runIO $ do
    vars <- EnvVars.parse
    Solutions.getInput vars.puzzlesPath Solutions.Day04

  let
    exampleAnswers = Day04.solve exampleInput
    puzzleAnswers = Day04.solve puzzleInput

  describe "part one" $ do
    it "works with example input" $ do
      fmap (.partOne) exampleAnswers `shouldBe` Right 18

    it "works with puzzle input" $ do
      fmap (.partOne) puzzleAnswers `shouldBe` Right 2336

  describe "part two" $ do
    it "works with example input" $ do
      fmap (.partTwo) exampleAnswers `shouldBe` Right 9

    it "works with puzzle input" $ do
      fmap (.partTwo) puzzleAnswers `shouldBe` Right 1831
