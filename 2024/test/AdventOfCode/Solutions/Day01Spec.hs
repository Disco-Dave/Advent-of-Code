module AdventOfCode.Solutions.Day01Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day01 qualified as Day01

import Data.Text (Text)
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, runIO, shouldBe)


exampleInput :: Text
exampleInput =
  Text.unlines
    [ "3   4"
    , "4   3"
    , "2   5"
    , "1   3"
    , "3   9"
    , "3   3"
    ]


spec :: Spec
spec = do
  (PuzzleInput puzzleInput) <- runIO $ do
    vars <- EnvVars.parse
    Solutions.getInput vars.puzzlesPath Solutions.Day01

  let
    exampleAnswers = Day01.solve exampleInput
    puzzleAnswers = Day01.solve puzzleInput

  describe "part one" $ do
    it "works with example input" $ do
      fmap (.partOne) exampleAnswers `shouldBe` Right 11

    it "works with puzzle input" $ do
      fmap (.partOne) puzzleAnswers `shouldBe` Right 1388114

  describe "part two" $ do
    it "works with example input" $ do
      fmap (.partTwo) exampleAnswers `shouldBe` Right 31

    it "works with puzzle input" $ do
      fmap (.partTwo) puzzleAnswers `shouldBe` Right 23529853
