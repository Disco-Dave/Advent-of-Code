module AdventOfCode.Solutions.Day03Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day03 qualified as Day03

import Data.Text (Text)
import Test.Hspec (Spec, describe, it, runIO, shouldBe)


exampleInput :: Text
exampleInput =
  "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))"


spec :: Spec
spec = do
  (PuzzleInput puzzleInput) <- runIO $ do
    vars <- EnvVars.parse
    Solutions.getInput vars.puzzlesPath Solutions.Day03

  let
    exampleAnswers = Day03.solve exampleInput
    puzzleAnswers = Day03.solve puzzleInput

  describe "part one" $ do
    it "works with example input" $ do
      fmap (.partOne) exampleAnswers `shouldBe` Right 161

    it "works with puzzle input" $ do
      fmap (.partOne) puzzleAnswers `shouldBe` Right 157621318

  describe "part two" $ do
    it "works with example input" $ do
      fmap (.partTwo) exampleAnswers `shouldBe` Right 48

    it "works with puzzle input" $ do
      fmap (.partTwo) puzzleAnswers `shouldBe` Right 79845780
