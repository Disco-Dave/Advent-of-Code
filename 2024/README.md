# 🎄 Advent of Code 2024 🎄 

[Advent of Code](https://adventofcode.com/) is an annual set of Christmas-themed computer programming challenges that follow an Advent calendar.

The programming puzzles cover a variety of skill sets and skill levels and can be solved using any programming language.

## Requirements

My solutions are completed using [Haskell](https://www.haskell.org/).

In order to hack on this project you will need the following software:
  - [ghc 9.8.2](https://downloads.haskell.org/~ghc/9.8.2/docs/users_guide/)
  - [cabal 3.12.1.0](https://cabal.readthedocs.io/en/3.12/getting-started.html)

It's recommended to install both `ghc` and `cabal` using [ghcup](https://www.haskell.org/ghcup/).

Alternatively, you may compile and run the project exclusively using [docker](https://docs.docker.com/).

## How to run (Native)

You may compile the project by running:
```
cabal build
```
You may also compile with all warnings upgraded to errors:
```
cabal build --flags pedantic
```

You may run the test-suite for the project by running:
```
cabal test
```

You may execute the project by running:
```
cabal run advent-of-code --
```

You may print the project's help text by running:
```
cabal run advent-of-code -- --help
```
Example output:
```
Usage: advent-of-code [-p|--parallel] [--day1] [--day2] [--day3] [--day4]
                      [--day5] [--day6] [--day7] [--day8] [--day9] [--day10]
                      [--day11] [--day12] [--day13] [--day14] [--day15]
                      [--day16] [--day17] [--day18] [--day19] [--day20]
                      [--day21] [--day22] [--day23] [--day24] [--day25]

  Solve puzzles for Advent of Code (https://adventofcode.com/). If no days are
  specified, then all 25 days are ran.

Available options:
  -h,--help                Show this help text
  -p,--parallel            Run the solutions in parallel
  --day1                   Solve the puzzle for day 1
  --day2                   Solve the puzzle for day 2
  --day3                   Solve the puzzle for day 3
  --day4                   Solve the puzzle for day 4
  --day5                   Solve the puzzle for day 5
  --day6                   Solve the puzzle for day 6
  --day7                   Solve the puzzle for day 7
  --day8                   Solve the puzzle for day 8
  --day9                   Solve the puzzle for day 9
  --day10                  Solve the puzzle for day 10
  --day11                  Solve the puzzle for day 11
  --day12                  Solve the puzzle for day 12
  --day13                  Solve the puzzle for day 13
  --day14                  Solve the puzzle for day 14
  --day15                  Solve the puzzle for day 15
  --day16                  Solve the puzzle for day 16
  --day17                  Solve the puzzle for day 17
  --day18                  Solve the puzzle for day 18
  --day19                  Solve the puzzle for day 19
  --day20                  Solve the puzzle for day 20
  --day21                  Solve the puzzle for day 21
  --day22                  Solve the puzzle for day 22
  --day23                  Solve the puzzle for day 23
  --day24                  Solve the puzzle for day 24
  --day25                  Solve the puzzle for day 25
```

## How to run (Docker)

You may compile the project by running:
```
docker build -t advent-of-code .
```

You may execute the project by running:
```
docker run -it --rm advent-of-code
```

You may print the project's help text by running:
```
docker run -it --rm advent-of-code --help
```
