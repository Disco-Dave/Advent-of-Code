module AdventOfCode.Solutions.Day03Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day03 qualified as Day03
import Data.Text (Text)
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, shouldBe)


exampleInput :: Text
exampleInput =
  Text.unlines
    [ "vJrwpWtwJgWrhcsFMMfFFhFp"
    , "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL"
    , "PmmdzqPrVvPwwTWBwg"
    , "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn"
    , "ttgJtRGJQctTZtZT"
    , "CrZsJsPPZsGzwwsLwLmpwMDw"
    ]


spec :: Spec
spec = do
  describe "part one" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day03.solve exampleInput
      answer.partOne `shouldBe` 157

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day03
      Right answer <- pure $ Day03.solve puzzleInput
      answer.partOne `shouldBe` 7997

  describe "part two" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day03.solve exampleInput
      answer.partTwo `shouldBe` 70

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day03
      Right answer <- pure $ Day03.solve puzzleInput
      answer.partTwo `shouldBe` 2545
