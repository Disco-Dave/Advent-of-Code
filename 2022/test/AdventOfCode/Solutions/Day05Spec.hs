module AdventOfCode.Solutions.Day05Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day05 qualified as Day05
import Data.Text (Text)
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, shouldBe)


exampleInput :: Text
exampleInput =
  Text.unlines
    [ "    [D]    "
    , "[N] [C]    "
    , "[Z] [M] [P]"
    , " 1   2   3 "
    , ""
    , "move 1 from 2 to 1"
    , "move 3 from 1 to 3"
    , "move 2 from 2 to 1"
    , "move 1 from 1 to 2"
    ]


spec :: Spec
spec = do
  describe "part one" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day05.solve exampleInput
      answer.partOne `shouldBe` "CMZ"

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day05
      Right answer <- pure $ Day05.solve puzzleInput
      answer.partOne `shouldBe` "RFFFWBPNS"

  describe "part two" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day05.solve exampleInput
      answer.partTwo `shouldBe` "MCD"

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day05
      Right answer <- pure $ Day05.solve puzzleInput
      answer.partTwo `shouldBe` "CQQBBJFCS"
