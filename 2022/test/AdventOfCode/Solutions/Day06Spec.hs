module AdventOfCode.Solutions.Day06Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day06 (PacketIndex (PacketIndex))
import AdventOfCode.Solutions.Day06 qualified as Day06
import Data.Foldable (for_)
import Test.Hspec (Spec, describe, it, shouldBe)


spec :: Spec
spec = do
  describe "part one" $ do
    describe "works with example input" $
      let examples =
            [ ("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 7)
            , ("bvwbjplbgvbhsrlpgdmjqwftvncz", 5)
            , ("nppdvjthqldpwncqszvftbrmjlhg", 6)
            , ("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 10)
            , ("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 11)
            ]
       in for_ examples $ \(input, expectedAnswer) ->
            it (show input <> " returns " <> show expectedAnswer) $ do
              Right answer <- pure $ Day06.solve input
              answer.partOne `shouldBe` PacketIndex (Just expectedAnswer)

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day06
      Right answer <- pure $ Day06.solve puzzleInput
      answer.partOne `shouldBe` PacketIndex (Just 1109)

  describe "part two" $ do
    describe "works with example input" $
      let examples =
            [ ("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 19)
            , ("bvwbjplbgvbhsrlpgdmjqwftvncz", 23)
            , ("nppdvjthqldpwncqszvftbrmjlhg", 23)
            , ("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 29)
            , ("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 26)
            ]
       in for_ examples $ \(input, expectedAnswer) ->
            it (show input <> " returns " <> show expectedAnswer) $ do
              Right answer <- pure $ Day06.solve input
              answer.partTwo `shouldBe` PacketIndex (Just expectedAnswer)

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day06
      Right answer <- pure $ Day06.solve puzzleInput
      answer.partTwo `shouldBe` PacketIndex (Just 3965)
