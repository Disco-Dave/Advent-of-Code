module AdventOfCode.Solutions.Day10Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day10 qualified as Day10
import Data.Text (Text)
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, shouldBe)


exampleInput :: Text
exampleInput =
  Text.unlines
    [ "addx 15"
    , "addx -11"
    , "addx 6"
    , "addx -3"
    , "addx 5"
    , "addx -1"
    , "addx -8"
    , "addx 13"
    , "addx 4"
    , "noop"
    , "addx -1"
    , "addx 5"
    , "addx -1"
    , "addx 5"
    , "addx -1"
    , "addx 5"
    , "addx -1"
    , "addx 5"
    , "addx -1"
    , "addx -35"
    , "addx 1"
    , "addx 24"
    , "addx -19"
    , "addx 1"
    , "addx 16"
    , "addx -11"
    , "noop"
    , "noop"
    , "addx 21"
    , "addx -15"
    , "noop"
    , "noop"
    , "addx -3"
    , "addx 9"
    , "addx 1"
    , "addx -3"
    , "addx 8"
    , "addx 1"
    , "addx 5"
    , "noop"
    , "noop"
    , "noop"
    , "noop"
    , "noop"
    , "addx -36"
    , "noop"
    , "addx 1"
    , "addx 7"
    , "noop"
    , "noop"
    , "noop"
    , "addx 2"
    , "addx 6"
    , "noop"
    , "noop"
    , "noop"
    , "noop"
    , "noop"
    , "addx 1"
    , "noop"
    , "noop"
    , "addx 7"
    , "addx 1"
    , "noop"
    , "addx -13"
    , "addx 13"
    , "addx 7"
    , "noop"
    , "addx 1"
    , "addx -33"
    , "noop"
    , "noop"
    , "noop"
    , "addx 2"
    , "noop"
    , "noop"
    , "noop"
    , "addx 8"
    , "noop"
    , "addx -1"
    , "addx 2"
    , "addx 1"
    , "noop"
    , "addx 17"
    , "addx -9"
    , "addx 1"
    , "addx 1"
    , "addx -3"
    , "addx 11"
    , "noop"
    , "noop"
    , "addx 1"
    , "noop"
    , "addx 1"
    , "noop"
    , "noop"
    , "addx -13"
    , "addx -19"
    , "addx 1"
    , "addx 3"
    , "addx 26"
    , "addx -30"
    , "addx 12"
    , "addx -1"
    , "addx 3"
    , "addx 1"
    , "noop"
    , "noop"
    , "noop"
    , "addx -9"
    , "addx 18"
    , "addx 1"
    , "addx 2"
    , "noop"
    , "noop"
    , "addx 9"
    , "noop"
    , "noop"
    , "noop"
    , "addx -1"
    , "addx 2"
    , "addx -37"
    , "addx 1"
    , "addx 3"
    , "noop"
    , "addx 15"
    , "addx -21"
    , "addx 22"
    , "addx -6"
    , "addx 1"
    , "noop"
    , "addx 2"
    , "addx 1"
    , "noop"
    , "addx -10"
    , "noop"
    , "noop"
    , "addx 20"
    , "addx 1"
    , "addx 2"
    , "addx 2"
    , "addx -6"
    , "addx -11"
    , "noop"
    , "noop"
    , "noop"
    ]


spec :: Spec
spec = do
  describe "part one" $ do
    it "works with puzzle input" $ do
      Right answer <- pure $ Day10.solve exampleInput
      answer.partOne `shouldBe` 13_140

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day10
      Right answer <- pure $ Day10.solve puzzleInput
      answer.partOne `shouldBe` 12_640

  describe "part two" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day10.solve exampleInput

      let expected =
            "\n"
              <> Text.unlines
                [ "##..##..##..##..##..##..##..##..##..##.."
                , "###...###...###...###...###...###...###."
                , "####....####....####....####....####...."
                , "#####.....#####.....#####.....#####....."
                , "######......######......######......####"
                , "#######.......#######.......#######....."
                ]

      Day10.crtToText answer.partTwo `shouldBe` expected

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day10
      Right answer <- pure $ Day10.solve puzzleInput

      let expected =
            "\n"
              <> Text.unlines
                [ "####.#..#.###..####.#....###....##.###.."
                , "#....#..#.#..#....#.#....#..#....#.#..#."
                , "###..####.###....#..#....#..#....#.#..#."
                , "#....#..#.#..#..#...#....###.....#.###.."
                , "#....#..#.#..#.#....#....#.#..#..#.#.#.."
                , "####.#..#.###..####.####.#..#..##..#..#."
                ]

      Day10.crtToText answer.partTwo `shouldBe` expected
