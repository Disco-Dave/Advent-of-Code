module AdventOfCode.Solutions.Day08Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day08 qualified as Day08
import Data.Text (Text)
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, shouldBe)


exampleInput :: Text
exampleInput =
  Text.unlines
    [ "30373"
    , "25512"
    , "65332"
    , "33549"
    , "35390"
    ]


spec :: Spec
spec = do
  describe "part one" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day08.solve exampleInput
      answer.partOne `shouldBe` 21

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day08
      Right answer <- pure $ Day08.solve puzzleInput
      answer.partOne `shouldBe` 1_782

  describe "part two" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day08.solve exampleInput
      answer.partTwo `shouldBe` 8

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day08
      Right answer <- pure $ Day08.solve puzzleInput
      answer.partTwo `shouldBe` 474_606
