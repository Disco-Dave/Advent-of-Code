module AdventOfCode.Solutions.Day14Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Answer qualified as Answer
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day14 qualified as Day14
import Test.Hspec (Spec, describe, it, pending, shouldBe)


spec :: Spec
spec = do
  describe "part one" $ do
    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day14
      Right answer <- pure $ Day14.solve puzzleInput
      answer.partOne `shouldBe` Answer.NotImplemented
      pending

  describe "part two" $ do
    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day14
      Right answer <- pure $ Day14.solve puzzleInput
      answer.partTwo `shouldBe` Answer.NotImplemented
      pending
