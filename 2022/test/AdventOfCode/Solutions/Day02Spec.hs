module AdventOfCode.Solutions.Day02Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day02 qualified as Day02
import Data.Text (Text)
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, shouldBe)


exampleInput :: Text
exampleInput =
  Text.unlines
    [ "A Y"
    , "B X"
    , "C Z"
    ]


spec :: Spec
spec = do
  describe "part one" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day02.solve exampleInput
      answer.partOne `shouldBe` 15

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day02

      Right answer <- pure $ Day02.solve puzzleInput
      answer.partOne `shouldBe` 10_310

  describe "part two" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day02.solve exampleInput
      answer.partTwo `shouldBe` 12

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day02

      Right answer <- pure $ Day02.solve puzzleInput
      answer.partTwo `shouldBe` 14_859
