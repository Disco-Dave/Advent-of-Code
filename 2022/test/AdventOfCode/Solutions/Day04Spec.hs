module AdventOfCode.Solutions.Day04Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day04 qualified as Day04
import Data.Text (Text)
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, shouldBe)


exampleInput :: Text
exampleInput =
  Text.unlines
    [ "2-4,6-8"
    , "2-3,4-5"
    , "5-7,7-9"
    , "2-8,3-7"
    , "6-6,4-6"
    , "2-6,4-8"
    ]


spec :: Spec
spec = do
  describe "part one" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day04.solve exampleInput
      answer.partOne `shouldBe` 2

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day04
      Right answer <- pure $ Day04.solve puzzleInput
      answer.partOne `shouldBe` 424

  describe "part two" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day04.solve exampleInput
      answer.partTwo `shouldBe` 4

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day04
      Right answer <- pure $ Day04.solve puzzleInput
      answer.partTwo `shouldBe` 804
