module AdventOfCode.Solutions.Day09Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day09 qualified as Day09
import Data.Text (Text)
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, shouldBe)


exampleInput :: Text
exampleInput =
  Text.unlines
    [ "R 4"
    , "U 4"
    , "L 3"
    , "D 1"
    , "R 4"
    , "D 1"
    , "L 5"
    , "R 2"
    ]


spec :: Spec
spec = do
  describe "part one" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day09.solve exampleInput
      answer.partOne `shouldBe` 13

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day09
      Right answer <- pure $ Day09.solve puzzleInput
      answer.partOne `shouldBe` 6642

  describe "part two" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day09.solve exampleInput
      answer.partTwo `shouldBe` 1

    it "works with second example input" $ do
      let secondExampleInput =
            Text.unlines
              [ "R 5"
              , "U 8"
              , "L 8"
              , "D 3"
              , "R 17"
              , "D 10"
              , "L 25"
              , "U 20"
              ]
      Right answer <- pure $ Day09.solve secondExampleInput
      answer.partTwo `shouldBe` 36

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day09
      Right answer <- pure $ Day09.solve puzzleInput
      answer.partTwo `shouldBe` 2765
