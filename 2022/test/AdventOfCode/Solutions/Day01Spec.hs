module AdventOfCode.Solutions.Day01Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day01 qualified as Day01
import Data.Text (Text)
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, shouldBe)


exampleInput :: Text
exampleInput =
  Text.unlines
    [ "1000"
    , "2000"
    , "3000"
    , ""
    , "4000"
    , ""
    , "5000"
    , "6000"
    , ""
    , "7000"
    , "8000"
    , "9000"
    , ""
    , "10000"
    ]


spec :: Spec
spec = do
  describe "part one" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day01.solve exampleInput
      answer.partOne `shouldBe` 24_000

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      PuzzleInput puzzleInput <- Solutions.getInput vars.puzzlesPath Solutions.Day01

      Right answer <- pure $ Day01.solve puzzleInput
      answer.partOne `shouldBe` 69_281

  describe "part two" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day01.solve exampleInput
      answer.partTwo `shouldBe` 45_000

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      PuzzleInput puzzleInput <- Solutions.getInput vars.puzzlesPath Solutions.Day01

      Right answer <- pure $ Day01.solve puzzleInput
      answer.partTwo `shouldBe` 20_1524
