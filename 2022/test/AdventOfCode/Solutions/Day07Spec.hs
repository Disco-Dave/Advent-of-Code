module AdventOfCode.Solutions.Day07Spec (spec) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.EnvVars qualified as EnvVars
import AdventOfCode.Solutions (PuzzleInput (..))
import AdventOfCode.Solutions qualified as Solutions
import AdventOfCode.Solutions.Day07 qualified as Day07
import Data.Text (Text)
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, shouldBe)


exampleInput :: Text
exampleInput =
  Text.unlines
    [ "$ cd /"
    , "$ ls"
    , "dir a"
    , "14848514 b.txt"
    , "8504156 c.dat"
    , "dir d"
    , "$ cd a"
    , "$ ls"
    , "dir e"
    , "29116 f"
    , "2557 g"
    , "62596 h.lst"
    , "$ cd e"
    , "$ ls"
    , "584 i"
    , "$ cd .."
    , "$ cd .."
    , "$ cd d"
    , "$ ls"
    , "4060174 j"
    , "8033020 d.log"
    , "5626152 d.ext"
    , "7214296 k"
    ]


spec :: Spec
spec = do
  describe "part one" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day07.solve exampleInput 
      answer.partOne `shouldBe` 95_437

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day07
      Right answer <- pure $ Day07.solve puzzleInput
      answer.partOne `shouldBe` 1_908_462

  describe "part two" $ do
    it "works with example input" $ do
      Right answer <- pure $ Day07.solve exampleInput 
      answer.partTwo `shouldBe` 24_933_642

    it "works with puzzle input" $ do
      vars <- EnvVars.parse
      (PuzzleInput puzzleInput) <- Solutions.getInput vars.puzzlesPath Solutions.Day07
      Right answer <- pure $ Day07.solve puzzleInput
      answer.partTwo `shouldBe` 3_979_145
