module AdventOfCode.Solutions.Day08 (solve) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Parsers qualified as Parsers
import Control.DeepSeq (NFData)
import Data.Attoparsec.Text qualified as Attoparsec
import Data.Char qualified as Char
import Data.Maybe (fromMaybe, mapMaybe)
import Data.Text (Text)
import Data.Vector (Vector, (!?))
import Data.Vector qualified as Vector
import Text.Read (readMaybe)


newtype NumberOfTrees = NumberOfTrees Int
  deriving (Num, Eq, Ord, NFData)


instance Show NumberOfTrees where
  show (NumberOfTrees numberOfTrees) =
    show numberOfTrees <> " trees"


newtype ScenicScore = ScenicScore Int
  deriving (Num, Eq, Ord, NFData)


instance Show ScenicScore where
  show (ScenicScore scenicScore) =
    show scenicScore <> " scenic score"


newtype TreeHeight = TreeHeight Int
  deriving (Show, Num, Eq, Ord)


newtype TreeGrid = TreeGrid (Vector (Vector TreeHeight))
  deriving (Show, Eq)


data GridSize = GridSize
  { columns :: Int
  , rows :: Int
  }
  deriving (Show, Eq)


data GridCoordinate = GridCoordinate
  { column :: Int
  , row :: Int
  }
  deriving (Show, Eq)


parseTreeGrid :: Text -> Either ParseError TreeGrid
parseTreeGrid =
  Parsers.parse $ do
    let parseTreeHeight = do
          rawHeight <- Attoparsec.satisfy Char.isDigit
          case readMaybe [rawHeight] of
            Nothing -> fail $ "invalid number detected: " <> [rawHeight]
            Just parsedHeight -> pure $ TreeHeight parsedHeight

        parseTreeRow = do
          heights <- Attoparsec.many1' parseTreeHeight
          pure $ Vector.fromList heights

    rows <- parseTreeRow `Attoparsec.sepBy` Attoparsec.endOfLine

    _ <- Attoparsec.endOfLine

    pure . TreeGrid $ Vector.fromList rows


gridSize :: TreeGrid -> GridSize
gridSize (TreeGrid grid) =
  let rows = Vector.length grid
      columns = maybe 0 Vector.length $ grid !? 0
   in GridSize{rows, columns}


lookupTree :: TreeGrid -> GridCoordinate -> Maybe TreeHeight
lookupTree (TreeGrid grid) GridCoordinate{row, column} = do
  rowOfTrees <- grid !? row
  rowOfTrees !? column


treesToTheLeft :: TreeGrid -> GridCoordinate -> [TreeHeight]
treesToTheLeft grid GridCoordinate{row, column} =
  let coordinates =
        [ GridCoordinate{row, column = c}
        | c <- [column - 1, column - 2 .. 0]
        ]
   in mapMaybe (lookupTree grid) coordinates


treesToTheRight :: TreeGrid -> GridCoordinate -> [TreeHeight]
treesToTheRight grid GridCoordinate{row, column} =
  let size = gridSize grid
      coordinates =
        [ GridCoordinate{row, column = c}
        | c <- [column + 1 .. size.columns - 1]
        ]
   in mapMaybe (lookupTree grid) coordinates


treesToTheBottom :: TreeGrid -> GridCoordinate -> [TreeHeight]
treesToTheBottom grid GridCoordinate{row, column} =
  let size = gridSize grid
      coordinates =
        [ GridCoordinate{row = r, column}
        | r <- [row + 1 .. size.rows - 1]
        ]
   in mapMaybe (lookupTree grid) coordinates


treesToTheTop :: TreeGrid -> GridCoordinate -> [TreeHeight]
treesToTheTop grid GridCoordinate{row, column} =
  let coordinates =
        [ GridCoordinate{row = r, column}
        | r <- [row - 1, row - 2 .. 0]
        ]
   in mapMaybe (lookupTree grid) coordinates


isTreeVisible :: TreeGrid -> GridCoordinate -> Bool
isTreeVisible grid coordinate =
  let targetTreeHeight = fromMaybe 0 $ lookupTree grid coordinate
      isVisibleFrom direction =
        all (< targetTreeHeight) $ direction grid coordinate
   in any
        isVisibleFrom
        [ treesToTheLeft
        , treesToTheRight
        , treesToTheBottom
        , treesToTheTop
        ]


takeWhileKeepingFirstFailure :: (a -> Bool) -> [a] -> [a]
takeWhileKeepingFirstFailure predicate items =
  let go input output =
        case input of
          (i : is)
            | predicate i -> go is (i : output)
            | otherwise -> i : output
          _ ->
            output
   in go items []


score :: TreeGrid -> GridCoordinate -> ScenicScore
score grid coordinate =
  let targetTreeHeight = fromMaybe 0 $ lookupTree grid coordinate
      treesWeCanSee direction =
        let trees = direction grid coordinate
         in length $ takeWhileKeepingFirstFailure (< targetTreeHeight) trees
   in ScenicScore . product $
        [ treesWeCanSee treesToTheLeft
        , treesWeCanSee treesToTheRight
        , treesWeCanSee treesToTheTop
        , treesWeCanSee treesToTheBottom
        ]


solvePartOne :: TreeGrid -> NumberOfTrees
solvePartOne grid =
  let GridSize{rows, columns} = gridSize grid
      coordinates =
        [ GridCoordinate{row, column}
        | row <- [0 .. rows - 1]
        , column <- [0 .. columns - 1]
        ]
   in NumberOfTrees . length $ filter (isTreeVisible grid) coordinates


solvePartTwo :: TreeGrid -> ScenicScore
solvePartTwo grid =
  let size = gridSize grid
      coordinates =
        [ GridCoordinate{row, column}
        | row <- [0 .. size.rows - 1]
        , column <- [0 .. size.columns - 1]
        ]
   in maximum $ fmap (score grid) coordinates


solve :: Text -> Either ParseError (Answer NumberOfTrees ScenicScore)
solve text = do
  treeGrid <- parseTreeGrid text

  Right $
    Answer
      { partOne = solvePartOne treeGrid
      , partTwo = solvePartTwo treeGrid
      }
