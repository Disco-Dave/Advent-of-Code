module AdventOfCode.Solutions.Day10
  ( SignalStrength (..)
  , Pixel (..)
  , Crt (..)
  , crtToText
  , solve
  )
where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Parsers qualified as Parsers
import Control.DeepSeq (NFData)
import Data.Attoparsec.Text qualified as Attoparsec
import Data.Foldable (foldl', toList)
import Data.Functor (($>), (<&>))
import Data.List.Split (chunksOf)
import Data.Maybe (mapMaybe)
import Data.Sequence (Seq, (|>))
import Data.Sequence qualified as Seq
import Data.Text (Text)
import Data.Text qualified as Text


newtype Registers = Registers
  { x :: Int
  }
  deriving (Show, Eq)


newtype SignalStrength = SignalStrength Int
  deriving (Eq, Ord, Num, NFData)


instance Show SignalStrength where
  show (SignalStrength signalStrength) =
    show signalStrength <> " signal strength"


data Cpu = Cpu
  { registers :: Registers
  , cycles :: Seq Registers
  }


data Instruction
  = NoOp
  | AddX Int
  deriving (Show, Eq)


data Pixel
  = DarkPixel
  | LitPixel
  deriving (Show, Eq, Ord)


newtype Crt = Crt
  { pixels :: [Pixel]
  }
  deriving (Eq, Ord)


crtToText :: Crt -> Text
crtToText Crt{pixels} =
  let pixelToText = \case
        DarkPixel -> '.'
        LitPixel -> '#'
      renderCrtLine = Text.pack . fmap pixelToText
      crtLines = take 6 $ renderCrtLine <$> chunksOf 40 pixels
   in "\n" <> Text.unlines crtLines


instance Show Crt where
  show = Text.unpack . crtToText


initializeCpu :: Cpu
initializeCpu =
  Cpu
    { registers =
        Registers
          { x = 1
          }
    , cycles = Seq.singleton (Registers 1)
    }


parseInstructions :: Text -> Either ParseError [Instruction]
parseInstructions =
  Parsers.parse $ do
    let parseNoOp =
          Attoparsec.string "noop" $> NoOp

        parseAddX = do
          _ <- Attoparsec.string "addx "
          fmap AddX Parsers.int

        parseInstruction =
          Attoparsec.choice
            [ parseNoOp
            , parseAddX
            ]

    instructions <- parseInstruction `Attoparsec.sepBy` Attoparsec.endOfLine

    _ <- Attoparsec.endOfLine

    pure instructions


simulateInstruction :: Instruction -> Cpu -> Cpu
simulateInstruction instruction cpu =
  case instruction of
    NoOp ->
      Cpu
        { registers = cpu.registers
        , cycles = cpu.cycles |> cpu.registers
        }
    AddX value ->
      let updatedRegisters =
            Registers
              { x = cpu.registers.x + value
              }
       in Cpu
            { registers = updatedRegisters
            , cycles = cpu.cycles |> cpu.registers |> updatedRegisters
            }


simulateInstructions :: [Instruction] -> Cpu -> Cpu
simulateInstructions instructions cpu =
  foldl' (flip simulateInstruction) cpu instructions


solvePartOne :: Cpu -> SignalStrength
solvePartOne cpu =
  let cyclesOfInterest = 20 : [60, 100 .. 220]
   in SignalStrength . sum . flip mapMaybe cyclesOfInterest $ \cycleIndex -> do
        registers <- Seq.lookup (cycleIndex - 1) cpu.cycles
        pure $ registers.x * cycleIndex


solvePartTwo :: Cpu -> Crt
solvePartTwo cpu =
  Crt $
    zip [0 ..] (toList cpu.cycles) <&> \(i, registers) ->
      let spriteLocations = [registers.x - 1, registers.x, registers.x + 1]
       in if (i `mod` 40) `elem` spriteLocations
            then LitPixel
            else DarkPixel


solve :: Text -> Either ParseError (Answer SignalStrength Crt)
solve text = do
  instructions <- parseInstructions text

  let cpu = simulateInstructions instructions initializeCpu

  Right $
    Answer
      { partOne = solvePartOne cpu
      , partTwo = solvePartTwo cpu
      }
