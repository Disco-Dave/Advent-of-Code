module AdventOfCode.Solutions.Day01 (solve) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Parsers qualified as Parsers
import Control.DeepSeq (NFData)
import Data.Attoparsec.Text qualified as Attoparsec
import Data.Coerce (coerce)
import Data.List (sortOn)
import Data.Maybe (listToMaybe)
import Data.Ord (Down (..))
import Data.Text (Text)
import Numeric.Natural (Natural)


newtype Calories = Calories Natural
  deriving (Num, Eq, Ord, NFData)


instance Show Calories where
  show (Calories calories) =
    show calories <> " Calories"


newtype FoodItem = FoodItem
  { calories :: Calories
  }
  deriving (Show, Eq, NFData)


newtype Elf = Elf
  { inventory :: [FoodItem]
  }
  deriving (Show, Eq, NFData)


parseElves :: Text -> Either ParseError [Elf]
parseElves =
  Parsers.parse $ do
    let parseFoodItem =
          FoodItem . Calories <$> Parsers.natural

        parseElf =
          fmap Elf (parseFoodItem `Attoparsec.sepBy` Attoparsec.endOfLine)

    elves <- parseElf `Attoparsec.sepBy` (Attoparsec.endOfLine *> Attoparsec.endOfLine)

    Attoparsec.endOfLine
    
    pure elves



newtype ElvesSortedByCaloriesDesc = ElvesSortedByCaloriesDesc [Elf]


totalCalories :: Elf -> Calories
totalCalories elf =
  sum . coerce @_ @[Calories] $ elf.inventory


sortElfs :: [Elf] -> ElvesSortedByCaloriesDesc
sortElfs elves =
  ElvesSortedByCaloriesDesc $
    sortOn (Down . totalCalories) elves


solvePartOne :: ElvesSortedByCaloriesDesc -> Calories
solvePartOne (ElvesSortedByCaloriesDesc elves) =
  maybe 0 totalCalories (listToMaybe elves)


solvePartTwo :: ElvesSortedByCaloriesDesc -> Calories
solvePartTwo (ElvesSortedByCaloriesDesc elves) =
  sum . fmap totalCalories $ take 3 elves


solve :: Text -> Either ParseError (Answer Calories Calories)
solve text = do
  elves <- parseElves text

  let sortedElves = sortElfs elves

  pure
    Answer
      { partOne = solvePartOne sortedElves
      , partTwo = solvePartTwo sortedElves
      }
