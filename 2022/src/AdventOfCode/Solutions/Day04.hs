module AdventOfCode.Solutions.Day04 (solve) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Parsers qualified as Parsers
import Control.DeepSeq (NFData)
import Data.Attoparsec.Text qualified as Attoparsec
import Data.Set (Set)
import Data.Set qualified as Set
import Data.Text (Text)
import Env.Generic (Generic)
import Numeric.Natural (Natural)


newtype CampSectionId = CampSectionId Natural
  deriving (Show, Eq, Ord, Num, Enum, NFData)


data CleanupAssignment = CleanupAssignment
  { start :: CampSectionId
  , end :: CampSectionId
  }
  deriving (Show, Eq, Generic)
instance NFData CleanupAssignment


data CleanupPair = CleanupPair
  { first :: CleanupAssignment
  , second :: CleanupAssignment
  }
  deriving (Show, Eq, Generic)
instance NFData CleanupPair


parseCleanupPairs :: Text -> Either ParseError [CleanupPair]
parseCleanupPairs =
  Parsers.parse $ do
    let parseCleanupAssignment = do
          start <- fmap CampSectionId Parsers.natural
          Attoparsec.skip (== '-')
          end <- fmap CampSectionId Parsers.natural
          pure CleanupAssignment{..}

        parseCleanupPair = do
          first <- parseCleanupAssignment
          Attoparsec.skip (== ',')
          second <- parseCleanupAssignment
          pure CleanupPair{..}

    pairs <- parseCleanupPair `Attoparsec.sepBy` Attoparsec.endOfLine

    Attoparsec.endOfLine

    pure pairs


cleanupSections :: CleanupAssignment -> Set CampSectionId
cleanupSections assignment =
  Set.fromList [assignment.start .. assignment.end]


doesOneRangeFullyContainTheOther :: CleanupPair -> Bool
doesOneRangeFullyContainTheOther cleanupPair =
  let firstSections = cleanupSections cleanupPair.first
      secondSections = cleanupSections cleanupPair.second
   in firstSections `Set.isSubsetOf` secondSections
        || secondSections `Set.isSubsetOf` firstSections


doesRangeOverlap :: CleanupPair -> Bool
doesRangeOverlap cleanupPair =
  let firstSections = cleanupSections cleanupPair.first
      secondSections = cleanupSections cleanupPair.second
      commonSections = Set.intersection firstSections secondSections
   in not $ Set.null commonSections


solvePartOne :: [CleanupPair] -> Int
solvePartOne =
  length . filter doesOneRangeFullyContainTheOther


solvePartTwo :: [CleanupPair] -> Int
solvePartTwo =
  length . filter doesRangeOverlap


solve :: Text -> Either ParseError (Answer Int Int)
solve input = do
  cleanupPairs <- parseCleanupPairs input

  pure
    Answer
      { partOne = solvePartOne cleanupPairs
      , partTwo = solvePartTwo cleanupPairs
      }
