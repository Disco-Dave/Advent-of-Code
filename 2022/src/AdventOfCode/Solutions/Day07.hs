module AdventOfCode.Solutions.Day07 (solve) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Parsers qualified as Parsers
import Control.DeepSeq (NFData)
import Data.Attoparsec.Text qualified as Attoparsec
import Data.Foldable (find, foldl')
import Data.Functor (($>))
import Data.List (sortOn, tails)
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Maybe (fromMaybe, mapMaybe)
import Data.String (IsString)
import Data.Text (Text)
import Numeric.Natural (Natural)


newtype FileName = FileName Text
  deriving (Eq, Ord, IsString)
  deriving (Show) via Text


data DirectoryToChangeTo
  = ChangeToParent
  | ChangeTo FileName
  deriving (Show, Eq, Ord)


newtype Bytes = Bytes Natural
  deriving (Eq, Ord, Num, NFData)


instance Show Bytes where
  show (Bytes bytes) =
    show bytes <> " bytes"


data File
  = Regular FileName Bytes
  | Directory FileName
  deriving (Show, Eq, Ord)


data Command
  = ChangeDirectory DirectoryToChangeTo
  | ListDirectory [File]
  deriving (Show, Eq, Ord)


type DirectoryPath = [FileName]


parseTerminalOutput :: Text -> Either ParseError [Command]
parseTerminalOutput =
  Parsers.parse $ do
    let parseDirectoryToChangeTo =
          Attoparsec.choice
            [ Attoparsec.string ".." $> ChangeToParent
            , ChangeTo . FileName <$> Attoparsec.takeTill Attoparsec.isEndOfLine
            ]

        parseChangeDirectory = do
          _ <- Attoparsec.string "$ cd "
          fmap ChangeDirectory parseDirectoryToChangeTo

        parseFile =
          let parseDirectory = do
                _ <- Attoparsec.string "dir "
                Directory . FileName <$> Attoparsec.takeTill Attoparsec.isEndOfLine
              parseRegular = do
                bytes <- fmap Bytes Parsers.natural
                _ <- Attoparsec.char ' '
                fileName <- FileName <$> Attoparsec.takeTill Attoparsec.isEndOfLine
                pure $ Regular fileName bytes
           in Attoparsec.choice [parseDirectory, parseRegular]

        parseListDirectory = do
          _ <- Attoparsec.string "$ ls"
          _ <- Attoparsec.endOfLine

          files <- parseFile `Attoparsec.sepBy` Attoparsec.endOfLine

          pure $ ListDirectory files

        parseCommand =
          Attoparsec.choice
            [ parseChangeDirectory
            , parseListDirectory
            ]

    commands <- parseCommand `Attoparsec.sepBy` Attoparsec.endOfLine

    _ <- Attoparsec.endOfLine

    pure commands


computeDirectorySizes :: [Command] -> Map DirectoryPath Bytes
computeDirectorySizes commands =
  let addBytes path bytes directories =
        let slashesRemoved = filter (/= "/") path
            pathPlusParents = tails slashesRemoved
         in foldl' (\d p -> Map.insertWith (+) p bytes d) directories pathPlusParents

      process (directories, currentWorkingDirectory) command =
        case command of
          ChangeDirectory ChangeToParent ->
            let newWorkingDirectory =
                  case currentWorkingDirectory of
                    (_ : workingDirectory) ->
                      workingDirectory
                    _ ->
                      []
             in (directories, newWorkingDirectory)
          ChangeDirectory (ChangeTo "/") ->
            (directories, [])
          ChangeDirectory (ChangeTo directory) ->
            (directories, directory : currentWorkingDirectory)
          ListDirectory files ->
            let totalSize =
                  sum . flip mapMaybe files $ \case
                    Regular _ size -> Just size
                    _ -> Nothing
             in ( addBytes currentWorkingDirectory totalSize directories
                , currentWorkingDirectory
                )
   in fst $ foldl' process (Map.empty, []) commands


solvePartOne :: Map DirectoryPath Bytes -> Bytes
solvePartOne sizes =
  sum . flip mapMaybe (Map.toList sizes) $ \(path, size) ->
    if path /= [] && size <= 100_000
      then Just size
      else Nothing


solvePartTwo :: Map DirectoryPath Bytes -> Bytes
solvePartTwo sizes =
  let usedSpace = fromMaybe 0 $ Map.lookup [] sizes
      freeSpace = 70_000_000 - usedSpace
      spaceNeededForUpdate = 30_000_000
   in if freeSpace > spaceNeededForUpdate
        then 0
        else
          let spaceNeeded = spaceNeededForUpdate - freeSpace
              directoriesSortedBySizeAsc = sortOn snd $ Map.toList sizes
           in maybe 0 snd $
                find (\(_, size) -> size >= spaceNeeded) directoriesSortedBySizeAsc


solve :: Text -> Either ParseError (Answer Bytes Bytes)
solve text = do
  commands <- parseTerminalOutput text

  let sizes = computeDirectorySizes commands

  pure $
    Answer
      { partOne = solvePartOne sizes
      , partTwo = solvePartTwo sizes
      }
