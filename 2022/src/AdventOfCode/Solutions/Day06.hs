module AdventOfCode.Solutions.Day06
  ( PacketIndex (..)
  , solve
  )
where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import Control.DeepSeq (NFData)
import Data.List.Split (divvy)
import Data.Maybe (listToMaybe, mapMaybe)
import Data.Set qualified as Set
import Data.String (IsString)
import Data.Text (Text)
import Data.Text qualified as Text


newtype Datastream = Datastream Text
  deriving (Show, Eq, IsString)


newtype StartOfPacketMarker = StartOfPacketMarker Text
  deriving (Show, Eq)


newtype PacketIndex = PacketIndex (Maybe Int)
  deriving (Eq, NFData)


instance Show PacketIndex where
  show (PacketIndex maybeIndex) =
    maybe "No start of packet marker found" show maybeIndex


parseDatastream :: Text -> Either ParseError Datastream
parseDatastream =
  Right . Datastream . Text.strip


findStartOfPacketMarker :: Int -> Datastream -> Maybe (Int, StartOfPacketMarker)
findStartOfPacketMarker width (Datastream text) =
  let chars = Text.unpack text

      pontentialPackets =
        divvy width 1 chars

      parseStartOfPacketMarker (index, packet) =
        let setOfChars = Set.fromList packet
         in if Set.size setOfChars == width
              then Just (index, StartOfPacketMarker (Text.pack packet))
              else Nothing

      parsedPackets =
        mapMaybe parseStartOfPacketMarker $ zip [width ..] pontentialPackets
   in listToMaybe parsedPackets


solvePartOne :: Datastream -> PacketIndex
solvePartOne =
  PacketIndex . fmap fst . findStartOfPacketMarker 4


solvePartTwo :: Datastream -> PacketIndex
solvePartTwo =
  PacketIndex . fmap fst . findStartOfPacketMarker 14


solve :: Text -> Either ParseError (Answer PacketIndex PacketIndex)
solve text = do
  rawDatastream <- parseDatastream text

  pure
    Answer
      { partOne = solvePartOne rawDatastream
      , partTwo = solvePartTwo rawDatastream
      }
