module AdventOfCode.Solutions.Day09
  ( NumberOfUniqueSpotsVisited (..)
  , solve
  )
where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Parsers qualified as Parsers
import Control.DeepSeq (NFData)
import Data.Attoparsec.Text qualified as Attoparsec
import Data.Coerce (coerce)
import Data.Foldable (foldl')
import Data.Function ((&))
import Data.Functor (($>))
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Monoid (Endo (..))
import Data.Set (Set)
import Data.Set qualified as Set
import Data.Text (Text)
import Numeric.Natural (Natural)


data Coordinate = Coordinate
  { column :: Int
  , row :: Int
  }
  deriving (Eq, Ord)


instance Show Coordinate where
  show coord =
    show (coord.column, coord.row)


data Knot = Knot
  { position :: Coordinate
  , positionsVisited :: Set Coordinate
  }
  deriving (Show, Eq)


type Rope = NonEmpty Knot


newtype NumberOfUniqueSpotsVisited = NumberOfUniqueSpotsVisited Natural
  deriving (Eq, Ord, Num, NFData)


instance Show NumberOfUniqueSpotsVisited where
  show (NumberOfUniqueSpotsVisited spots) =
    show spots <> " unique spots"


data MoveDirection
  = MoveRight
  | MoveUp
  | MoveLeft
  | MoveDown
  deriving (Show, Eq)


data Move = Move
  { direction :: MoveDirection
  , steps :: Int
  }
  deriving (Show, Eq)


parseMoves :: Text -> Either ParseError [Move]
parseMoves =
  Parsers.parse $ do
    let parseDirection =
          Attoparsec.choice
            [ Attoparsec.char 'R' $> MoveRight
            , Attoparsec.char 'U' $> MoveUp
            , Attoparsec.char 'L' $> MoveLeft
            , Attoparsec.char 'D' $> MoveDown
            ]

        parseSteps = do
          rawSteps <- Parsers.int
          if rawSteps > 0
            then pure rawSteps
            else fail "Moves must be for one or more steps"

        parseMove = do
          direction <- parseDirection
          Attoparsec.skipSpace
          steps <- parseSteps
          pure Move{..}

    moves <- parseMove `Attoparsec.sepBy` Attoparsec.endOfLine

    _ <- Attoparsec.endOfLine

    pure moves


makeRope :: Int -> Rope
makeRope numberOfExtraKnots =
  let origin = Coordinate 0 0
      freshKnot = Knot origin (Set.singleton origin)
      extraKnots = replicate numberOfExtraKnots freshKnot
   in freshKnot :| extraKnots


updatePosition :: Knot -> Coordinate -> Knot
updatePosition knot newPosition =
  Knot
    { position = newPosition
    , positionsVisited = Set.insert newPosition knot.positionsVisited
    }


moveKnotRight :: Knot -> Knot
moveKnotRight knot =
  updatePosition knot $
    Coordinate
      { row = knot.position.row
      , column = knot.position.column + 1
      }


moveKnotUp :: Knot -> Knot
moveKnotUp knot =
  updatePosition knot $
    Coordinate
      { row = knot.position.row + 1
      , column = knot.position.column
      }


moveKnotLeft :: Knot -> Knot
moveKnotLeft knot =
  updatePosition knot $
    Coordinate
      { row = knot.position.row
      , column = knot.position.column - 1
      }


moveKnotDown :: Knot -> Knot
moveKnotDown knot =
  updatePosition knot $
    Coordinate
      { row = knot.position.row - 1
      , column = knot.position.column
      }


moveKnotUpRight :: Knot -> Knot
moveKnotUpRight knot =
  updatePosition knot $
    Coordinate
      { row = knot.position.row + 1
      , column = knot.position.column + 1
      }


moveKnotDownRight :: Knot -> Knot
moveKnotDownRight knot =
  updatePosition knot $
    Coordinate
      { row = knot.position.row - 1
      , column = knot.position.column + 1
      }


moveKnotDownLeft :: Knot -> Knot
moveKnotDownLeft knot =
  updatePosition knot $
    Coordinate
      { row = knot.position.row - 1
      , column = knot.position.column - 1
      }


moveKnotUpLeft :: Knot -> Knot
moveKnotUpLeft knot =
  updatePosition knot $
    Coordinate
      { row = knot.position.row + 1
      , column = knot.position.column - 1
      }


moveTailKnot :: Knot -> Knot -> Knot
moveTailKnot headKnot tailKnot =
  let headColumn = headKnot.position.column
      headRow = headKnot.position.row

      tailColumn = tailKnot.position.column
      tailRow = tailKnot.position.row

      horizontalDistance =
        headColumn - tailColumn

      verticalDistance =
        headRow - tailRow

      moveKnot
        | horizontalDistance == 0 && verticalDistance >= 2 = moveKnotUp
        | horizontalDistance >= 2 && verticalDistance == 0 = moveKnotRight
        | horizontalDistance == 0 && verticalDistance <= -2 = moveKnotDown
        | horizontalDistance <= -2 && verticalDistance == 0 = moveKnotLeft
        | horizontalDistance >= 2 && verticalDistance >= 1 = moveKnotUpRight
        | horizontalDistance >= 1 && verticalDistance >= 2 = moveKnotUpRight
        | horizontalDistance >= 2 && verticalDistance <= -1 = moveKnotDownRight
        | horizontalDistance >= 1 && verticalDistance <= -2 = moveKnotDownRight
        | horizontalDistance <= -2 && verticalDistance <= -1 = moveKnotDownLeft
        | horizontalDistance <= -1 && verticalDistance <= -2 = moveKnotDownLeft
        | horizontalDistance <= -2 && verticalDistance >= 1 = moveKnotUpLeft
        | horizontalDistance <= -1 && verticalDistance >= 2 = moveKnotUpLeft
        | otherwise = id
   in moveKnot tailKnot


moveRopeOnce :: MoveDirection -> Rope -> Rope
moveRopeOnce direction rope =
  let (headKnot :| tailKnots) = rope
      updatedHeadKnot =
        headKnot & case direction of
          MoveRight -> moveKnotRight
          MoveUp -> moveKnotUp
          MoveLeft -> moveKnotLeft
          MoveDown -> moveKnotDown
   in NonEmpty.scanl moveTailKnot updatedHeadKnot tailKnots


moveRope :: Move -> Rope -> Rope
moveRope move =
  appEndo . mconcat . coerce . replicate move.steps $
    moveRopeOnce move.direction


simulateMoves :: [Move] -> Rope -> Rope
simulateMoves moves rope =
  foldl' (flip moveRope) rope moves


solvePartOne :: [Move] -> NumberOfUniqueSpotsVisited
solvePartOne moves =
  let rope = simulateMoves moves (makeRope 1)
   in case rope of
        (_ :| (knot : _)) ->
          fromIntegral $ Set.size knot.positionsVisited
        _ -> 0


solvePartTwo :: [Move] -> NumberOfUniqueSpotsVisited
solvePartTwo moves =
  let rope = simulateMoves moves (makeRope 9)
   in fromIntegral $ Set.size (NonEmpty.last rope).positionsVisited


solve :: Text -> Either ParseError (Answer NumberOfUniqueSpotsVisited NumberOfUniqueSpotsVisited)
solve text = do
  moves <- parseMoves text

  Right $
    Answer
      { partOne = solvePartOne moves
      , partTwo = solvePartTwo moves
      }
