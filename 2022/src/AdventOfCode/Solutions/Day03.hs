module AdventOfCode.Solutions.Day03 (solve) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Parsers qualified as Parsers
import Control.DeepSeq (NFData)
import Data.Attoparsec.Text qualified as Attoparsec
import Data.Bifunctor (Bifunctor (bimap))
import Data.Coerce (coerce)
import Data.Foldable (Foldable (foldl'))
import Data.List.Split (chunksOf)
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Maybe (fromMaybe, listToMaybe)
import Data.Set qualified as Set
import Data.Text (Text)
import Numeric.Natural (Natural)


newtype ItemId = ItemId Char
  deriving (Show, Eq, Ord, Enum, NFData)


newtype Rucksack = Rucksack [ItemId]
  deriving (Show, Eq, NFData)


newtype Priority = Priority Natural
  deriving (Eq, Ord, Num, Enum, NFData)


instance Show Priority where
  show (Priority priority) =
    show priority <> " priority"


priorityMap :: Map ItemId Priority
priorityMap =
  let itemIds = coerce $ ['a' .. 'z'] <> ['A' .. 'Z']
      priorities = [1 .. 52]
   in Map.fromList $ zip itemIds priorities


parseRucksacks :: Text -> Either ParseError [Rucksack]
parseRucksacks =
  Parsers.parse $ do
    let validLetters =
          Set.fromList $ ['a' .. 'z'] <> ['A' .. 'Z']

        parseItemId =
          ItemId <$> Attoparsec.satisfy (`Set.member` validLetters)

        parseRucksack = do
          itemIds <- Attoparsec.many1' parseItemId
          if even (length itemIds)
            then pure $ Rucksack itemIds
            else fail "The items in the rucksack must be even."

    rucksack <- parseRucksack `Attoparsec.sepBy` Attoparsec.endOfLine

    Attoparsec.endOfLine

    pure rucksack


prioritizeItem :: ItemId -> Priority
prioritizeItem itemId =
  fromMaybe 0 $ Map.lookup itemId priorityMap


findOutOfPlaceItems :: Rucksack -> [ItemId]
findOutOfPlaceItems (Rucksack items) =
  let (firstHalf, secondHalf) =
        bimap Set.fromList Set.fromList $
          splitAt (length items `div` 2) items
   in Set.toList $
        Set.intersection firstHalf secondHalf


solvePartOne :: [Rucksack] -> Priority
solvePartOne =
  let sumRucksack =
        sum . fmap prioritizeItem . findOutOfPlaceItems
   in sum . fmap sumRucksack


findBadge :: [Rucksack] -> Maybe ItemId
findBadge rucksacks = do
  let itemSets = fmap (Set.fromList . coerce) rucksacks

  commonItems <-
    case itemSets of
      (firstRucksack : otherRucksacks) ->
        Just $ foldl' Set.intersection firstRucksack otherRucksacks
      _ -> Nothing

  listToMaybe $ Set.toList commonItems


solvePartTwo :: [Rucksack] -> Priority
solvePartTwo rucksacks =
  sum $ maybe 0 prioritizeItem . findBadge <$> chunksOf 3 rucksacks


solve :: Text -> Either ParseError (Answer Priority Priority)
solve text = do
  rucksacks <- parseRucksacks text

  pure
    Answer
      { partOne = solvePartOne rucksacks
      , partTwo = solvePartTwo rucksacks
      }
