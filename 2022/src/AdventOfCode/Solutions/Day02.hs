module AdventOfCode.Solutions.Day02 (solve) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Parsers qualified as Parsers
import Control.DeepSeq (NFData)
import Data.Attoparsec.Text qualified as Attoparsec
import Data.Functor (($>))
import Data.Text (Text)
import Env.Generic (Generic)
import Numeric.Natural (Natural)


data Shape
  = Rock
  | Paper
  | Scissors
  deriving (Show, Eq, Generic)
instance NFData Shape


data Round = Round
  { opponent :: Shape
  , you :: Shape
  }
  deriving (Show, Eq, Generic)
instance NFData Round


data Winner
  = Opponent
  | You
  | Draw
  deriving (Show, Eq, Generic)
instance NFData Winner


newtype Points = Points Natural
  deriving (Eq, Ord, Num, NFData)


instance Show Points where
  show (Points points) =
    show points <> " points"


parseRounds :: Text -> Either ParseError [Round]
parseRounds =
  Parsers.parse $ do
    let parseShape =
          let parseRock =
                Attoparsec.choice
                  [ Attoparsec.char 'A'
                  , Attoparsec.char 'X'
                  ]
                  $> Rock
              parsePaper =
                Attoparsec.choice
                  [ Attoparsec.char 'B'
                  , Attoparsec.char 'Y'
                  ]
                  $> Paper
              parseScissors =
                Attoparsec.choice
                  [ Attoparsec.char 'C'
                  , Attoparsec.char 'Z'
                  ]
                  $> Scissors
           in Attoparsec.choice
                [ parseRock
                , parsePaper
                , parseScissors
                ]

        parseRound = do
          opponent <- parseShape
          Attoparsec.skipSpace
          you <- parseShape
          pure Round{..}

    rounds <- parseRound `Attoparsec.sepBy` Attoparsec.endOfLine

    Attoparsec.endOfLine
    
    pure rounds


roundWinner :: Round -> Winner
roundWinner = \case
  Round Rock Paper -> You
  Round Rock Scissors -> Opponent
  Round Paper Rock -> Opponent
  Round Paper Scissors -> You
  Round Scissors Rock -> You
  Round Scissors Paper -> Opponent
  _ -> Draw


scoreShape :: Shape -> Points
scoreShape = \case
  Rock -> 1
  Paper -> 2
  Scissors -> 3


scoreWinner :: Winner -> Points
scoreWinner = \case
  Opponent -> 0
  Draw -> 3
  You -> 6


scoreRound :: Round -> Points
scoreRound normalRound =
  scoreShape normalRound.you + scoreWinner (roundWinner normalRound)


solvePartOne :: [Round] -> Points
solvePartOne =
  sum . fmap scoreRound


data CorrectedRound = CorrectedRound
  { opponent :: Shape
  , winner :: Winner
  }


correctRound :: Round -> CorrectedRound
correctRound normalRound =
  CorrectedRound
    { opponent = normalRound.opponent
    , winner =
        case normalRound.you of
          Rock -> Opponent
          Paper -> Draw
          Scissors -> You
    }


yourCorrectedShape :: CorrectedRound -> Shape
yourCorrectedShape correctedRound =
  case correctedRound of
    CorrectedRound _ Draw -> correctedRound.opponent
    CorrectedRound Paper You -> Scissors
    CorrectedRound Scissors You -> Rock
    CorrectedRound Rock You -> Paper
    CorrectedRound Paper Opponent -> Rock
    CorrectedRound Scissors Opponent -> Paper
    CorrectedRound Rock Opponent -> Scissors


scoreCorrectedRound :: CorrectedRound -> Points
scoreCorrectedRound correctedRound =
  let yourShape = yourCorrectedShape correctedRound
   in scoreShape yourShape + scoreWinner correctedRound.winner


solvePartTwo :: [Round] -> Points
solvePartTwo =
  sum . fmap (scoreCorrectedRound . correctRound)


solve :: Text -> Either ParseError (Answer Points Points)
solve text = do
  rounds <- parseRounds text

  pure
    Answer
      { partOne = solvePartOne rounds
      , partTwo = solvePartTwo rounds
      }
