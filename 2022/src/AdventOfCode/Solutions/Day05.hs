module AdventOfCode.Solutions.Day05 (solve) where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Parsers qualified as Parsers
import Control.DeepSeq (NFData)
import Control.Monad (replicateM, replicateM_)
import Control.Monad.State.Strict (StateT (..))
import Control.Monad.State.Strict qualified as State
import Data.Attoparsec.Text qualified as Attoparsec
import Data.Coerce (coerce)
import Data.Foldable (Foldable (foldl'))
import Data.Function ((&))
import Data.List (transpose)
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Maybe (catMaybes, fromMaybe, mapMaybe)
import Data.String (IsString)
import Data.Text (Text)
import Data.Text qualified as Text
import Numeric.Natural (Natural)


newtype StackId = StackId Natural
  deriving (Show, Eq, Ord, Num, Enum, Real, Integral)


newtype CrateId = CrateId Char
  deriving (Show, Eq, Ord, Enum)


newtype CrateIds = CrateIds Text
  deriving (IsString, Eq, Ord, NFData)


instance Show CrateIds where
  show (CrateIds crateIds) =
    Text.unpack crateIds


newtype CrateStack = CrateStack [CrateId]
  deriving (Show, Eq, Ord)


newtype Cargo = Cargo (Map StackId CrateStack)
  deriving (Show, Eq)


data MoveInstruction = MoveInstruction
  { quantity :: Int
  , from :: StackId
  , to :: StackId
  }
  deriving (Show, Eq)


parseCargoAndInstructions :: Text -> Either ParseError (Cargo, [MoveInstruction])
parseCargoAndInstructions =
  Parsers.parse $ do
    let parseCrateRows =
          let parseCrate = do
                Attoparsec.skip (== '[')
                crateId <- Attoparsec.anyChar
                Attoparsec.skip (== ']')
                pure $ CrateId crateId

              parseEmptyCrateSpot = do
                replicateM_ 3 $ Attoparsec.skip (== ' ')

              parseCrateSpot =
                Attoparsec.choice
                  [ fmap Just parseCrate
                  , Nothing <$ parseEmptyCrateSpot
                  ]

              parseCrateRow =
                parseCrateSpot `Attoparsec.sepBy` Attoparsec.char ' '
           in parseCrateRow `Attoparsec.sepBy` Attoparsec.endOfLine

        parseStackIds =
          let parseStackId = do
                Attoparsec.skip (== ' ')
                stackId <- Parsers.natural
                Attoparsec.skip (== ' ')
                pure $ StackId stackId
           in parseStackId `Attoparsec.sepBy` Attoparsec.char ' '

        parseInstruction = do
          _ <- Attoparsec.string "move "
          quantity <- Parsers.int

          _ <- Attoparsec.string " from "
          from <- fmap StackId Parsers.natural

          _ <- Attoparsec.string " to "
          to <- fmap StackId Parsers.natural

          pure MoveInstruction{..}

    crateRows <- parseCrateRows
    stackIds <- parseStackIds

    let cargo =
          let stacks = CrateStack . catMaybes <$> transpose crateRows
           in Map.fromList $ zip stackIds stacks

    Attoparsec.endOfLine
    Attoparsec.endOfLine

    instructions <-
      parseInstruction `Attoparsec.sepBy` Attoparsec.endOfLine

    Attoparsec.endOfLine

    pure (Cargo cargo, instructions)


removeCrateFromTop :: CrateStack -> Maybe (CrateId, CrateStack)
removeCrateFromTop (CrateStack crates) =
  case crates of
    topCrate : otherCrates ->
      Just (topCrate, CrateStack otherCrates)
    _ ->
      Nothing


addCrateToTop :: CrateId -> CrateStack -> CrateStack
addCrateToTop crate (CrateStack crates) =
  CrateStack $ crate : crates


newtype Crane = Crane
  { stackCrates :: [CrateId] -> CrateStack -> CrateStack
  }


crateMover9000 :: Crane
crateMover9000 =
  Crane $ \crates stack ->
    foldl' (flip addCrateToTop) stack crates


crateMover9001 :: Crane
crateMover9001 =
  Crane $ \crates stack ->
    foldr addCrateToTop stack crates


executeInstruction :: Crane -> MoveInstruction -> Cargo -> Cargo
executeInstruction Crane{stackCrates} instruction (Cargo cargo) =
  fromMaybe (Cargo cargo) $ do
    fromStack <- Map.lookup instruction.from cargo
    toStack <- Map.lookup instruction.to cargo

    (removedCrates, updatedFromStack) <-
      flip State.runStateT fromStack $
        replicateM instruction.quantity (StateT removeCrateFromTop)

    let updatedToStack =
          stackCrates removedCrates toStack

        updatedCargo =
          cargo
            & Map.insert instruction.from updatedFromStack
            & Map.insert instruction.to updatedToStack

    Just $ Cargo updatedCargo


executeInstructions :: Crane -> Cargo -> [MoveInstruction] -> Cargo
executeInstructions crane =
  foldl' (flip (executeInstruction crane))


readTopCrates :: Cargo -> CrateIds
readTopCrates (Cargo cargo) =
  let stacksInOrder = snd <$> Map.toAscList cargo
      topCrates = mapMaybe (fmap fst . removeCrateFromTop) stacksInOrder
   in CrateIds . Text.pack $ coerce topCrates


solvePartOne :: Cargo -> [MoveInstruction] -> CrateIds
solvePartOne cargo =
  readTopCrates . executeInstructions crateMover9000 cargo


solvePartTwo :: Cargo -> [MoveInstruction] -> CrateIds
solvePartTwo cargo =
  readTopCrates . executeInstructions crateMover9001 cargo


solve :: Text -> Either ParseError (Answer CrateIds CrateIds)
solve text = do
  (cargo, moveInstructions) <- parseCargoAndInstructions text

  Right $
    Answer
      { partOne = solvePartOne cargo moveInstructions
      , partTwo = solvePartTwo cargo moveInstructions
      }
