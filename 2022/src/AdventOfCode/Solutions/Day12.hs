module AdventOfCode.Solutions.Day12 (solve) where

import AdventOfCode.Answer (Answer (..), NotImplemented (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Parsers qualified as Parsers

import Control.Monad.ST qualified as ST
import Data.Attoparsec.Text (Parser)
import Data.Attoparsec.Text qualified as Attoparsec
import Data.Char qualified as Char
import Data.Function (on)
import Data.Graph (Graph)
import Data.Graph qualified as Graph
import Data.List (sort)
import Data.Maybe (catMaybes, fromMaybe, listToMaybe, mapMaybe)
import Data.STRef qualified as STRef
import Data.Text (Text)
import Data.Traversable (for)
import Data.Vector (Vector, (!?))
import Data.Vector qualified as Vector
import GHC.Natural (Natural)

import Debug.Trace


newtype Elevation = Elevation {value :: Natural}
  deriving (Show, Eq, Ord, Enum, Num, Integral, Real)


data Tile
  = Start
  | Path Elevation
  | End
  deriving (Show, Eq, Ord)


elevationOfTile :: Tile -> Maybe Elevation
elevationOfTile = \case
  Path elevation -> Just elevation
  _ -> Nothing


parseTile :: Parser Tile
parseTile = do
  char <- Attoparsec.anyChar

  case char of
    'S' ->
      pure Start
    'E' ->
      pure End
    _
      | Char.isAlpha char && Char.isLower char ->
          pure . Path . fromIntegral $ (fromEnum char - fromEnum 'a')
      | otherwise ->
          fail $ "Unrecognized tile: " <> [char]


newtype HeightMap = HeightMap (Vector (Vector Tile))
  deriving (Show, Eq)


type X = Int
type Y = Int
type Coordinates = (X, Y)


lookupTile :: (X, Y) -> HeightMap -> Maybe Tile
lookupTile (x, y) (HeightMap vector) = do
  row <- vector !? y
  row !? x


parseHeightMap :: Text -> Either ParseError HeightMap
parseHeightMap =
  Parsers.parse $ do
    let
      parseRow =
        Vector.fromList <$> Attoparsec.many1' parseTile

    vector <-
      fmap Vector.fromList (parseRow `Attoparsec.sepBy` Attoparsec.endOfLine)

    pure $ HeightMap vector


data HeightGraph = HeightGraph
  { graph :: Graph
  , nodeFromVertex :: Graph.Vertex -> (Tile, Coordinates, [Coordinates])
  , vertexFromKey :: Coordinates -> Maybe Graph.Vertex
  , start :: Maybe Graph.Vertex
  , end :: Maybe Graph.Vertex
  }


instance Show HeightGraph where show = show . (.graph)
instance Eq HeightGraph where (==) = on (==) (.graph)
instance Ord HeightGraph where compare = on compare (.graph)


generateGraph :: HeightMap -> HeightGraph
generateGraph heightMap@(HeightMap vector) = do
  let
    tileEdges (x, y) elevation =
      let
        edgesToCheck =
          [ (x - 1, y - 1)
          , (x, y - 1)
          , (x - 1, y)
          , (x + 1, y + 1)
          , (x, y + 1)
          , (x + 1, y)
          , (x - 1, y + 1)
          , (x + 1, y - 1)
          ]

        canTravel to =
          elevation <= to + 1
       in
        flip mapMaybe edgesToCheck $ \edge -> do
          tile <- lookupTile edge heightMap
          to <- elevationOfTile tile
          if canTravel to
            then Just edge
            else Nothing

  ST.runST $ do
    startRef <- STRef.newSTRef Nothing
    endRef <- STRef.newSTRef Nothing

    rootRef <- STRef.newSTRef Nothing

    edges <-
      fmap concat . for (zip [0 ..] (Vector.toList vector)) $ \(y, row) ->
        fmap catMaybes . for (zip [0 ..] (Vector.toList row)) $ \(x, tile) ->
          case tile of
            Start -> do
              STRef.writeSTRef startRef (Just (x, y))

              let
                elevation =
                  fromMaybe 0 $ elevationOfTile =<< lookupTile (x + 1, y) heightMap

              STRef.writeSTRef rootRef $ Just (tile, (x, y), tileEdges (x, y) elevation)

              pure Nothing
            End -> do
              STRef.writeSTRef endRef (Just (x, y))

              let
                elevation =
                  fromMaybe 0 $ elevationOfTile =<< lookupTile (x - 1, y) heightMap

              pure $ Just (tile, (x, y), tileEdges (x, y) elevation)
            Path elevation ->
              pure $ Just (tile, (x, y), tileEdges (x, y) elevation)

    root <- maybe [] pure <$> STRef.readSTRef rootRef

    let
      (graph, nodeFromVertex, vertexFromKey) = Graph.graphFromEdges (edges <> root)

    start <- (>>= vertexFromKey) <$> STRef.readSTRef startRef
    end <- (>>= vertexFromKey) <$> STRef.readSTRef endRef

    pure HeightGraph{..}


solvePartOne :: HeightGraph -> Int
solvePartOne heightGraph =
  fromMaybe 0 $ do
    let end = fromMaybe 0 heightGraph.end

        paths = traceShowId $ Graph.dfs heightGraph.graph $ traceShowId [end]

        shortestPath = listToMaybe . traceShowId . sort $ fmap length paths
     in shortestPath


solve :: Text -> Either ParseError (Answer Int NotImplemented)
solve input = do
  heightMap <- parseHeightMap input
  let heightGraph = generateGraph heightMap

  Right $
    Answer
      { partOne = solvePartOne heightGraph
      , partTwo = NotImplemented
      }
