module AdventOfCode.Solutions.Day11
  ( MonkeyBusinessLevel (..)
  , solve
  )
where

import AdventOfCode.Answer (Answer (..))
import AdventOfCode.Parsers (ParseError)
import AdventOfCode.Parsers qualified as Parsers
import Control.Monad (replicateM_)
import Control.Monad.ST qualified as ST
import Control.Monad.Trans.Class (MonadTrans (lift))
import Control.Monad.Trans.Maybe (MaybeT (..))
import Data.Attoparsec.Text qualified as Attoparsec
import Data.Coerce (coerce)
import Data.Foldable (for_)
import Data.Functor (($>))
import Data.List (sortOn)
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Maybe (fromMaybe)
import Data.Ord (Down (..))
import Data.STRef.Strict qualified as STRef
import Data.Sequence (Seq, (|>))
import Data.Sequence qualified as Seq
import Data.Text (Text)


newtype MonkeyBusinessLevel = MonkeyBusinessLevel Integer
  deriving (Eq, Ord, Num)


instance Show MonkeyBusinessLevel where
  show (MonkeyBusinessLevel level) =
    show level <> " monkey business level"


newtype MonkeyId = MonkeyId Integer
  deriving (Show, Eq, Ord, Num)


newtype WorryLevel = WorryLevel Integer
  deriving (Show, Eq, Ord, Num, Enum, Real, Integral)


data Monkey = Monkey
  { items :: Seq WorryLevel
  , operation :: WorryLevel -> WorryLevel
  , inspectionCount :: Integer
  , divisor :: WorryLevel
  , trueMonkeyId :: MonkeyId
  , falseMonkeyId :: MonkeyId
  }


instance Show Monkey where
  show monkey =
    show monkey.items


type Monkeys = Map MonkeyId Monkey


parseMonkeys :: Text -> Either ParseError Monkeys
parseMonkeys =
  Parsers.parse $ do
    let parseMonkeyId = do
          _ <- Attoparsec.string "Monkey "
          monkeyId <- Parsers.integer
          _ <- Attoparsec.char ':'
          Attoparsec.endOfLine
          pure $ MonkeyId monkeyId

        parseStartingItems = do
          replicateM_ 2 (Attoparsec.char ' ')
          _ <- Attoparsec.string "Starting items: "
          worryLevels <- Parsers.integer `Attoparsec.sepBy` Attoparsec.string ", "
          Attoparsec.endOfLine
          pure . Seq.fromList $ coerce worryLevels

        parseOperation = do
          replicateM_ 2 (Attoparsec.char ' ')
          _ <- Attoparsec.string "Operation: new = old "

          mathOperation <-
            Attoparsec.choice
              [ Attoparsec.char '*' $> (*)
              , Attoparsec.char '+' $> (+)
              ]

          _ <- Attoparsec.char ' '

          maybeLeftSide <-
            Attoparsec.choice
              [ fmap (Just . WorryLevel) Parsers.integer
              , Attoparsec.string "old" $> Nothing
              ]

          Attoparsec.endOfLine

          pure $ \old ->
            old `mathOperation` fromMaybe old maybeLeftSide

        parseTest = do
          replicateM_ 2 (Attoparsec.char ' ')
          _ <- Attoparsec.string "Test: divisible by "
          divisibleBy <- fmap WorryLevel Parsers.integer

          Attoparsec.endOfLine

          replicateM_ 4 (Attoparsec.char ' ')
          _ <- Attoparsec.string "If true: throw to monkey "
          trueMonkeyId <- fmap MonkeyId Parsers.integer

          Attoparsec.endOfLine

          replicateM_ 4 (Attoparsec.char ' ')
          _ <- Attoparsec.string "If false: throw to monkey "
          falseMonkeyId <- fmap MonkeyId Parsers.integer

          pure (divisibleBy, trueMonkeyId, falseMonkeyId)

        parseMonkey = do
          monkeyId <- parseMonkeyId
          items <- parseStartingItems
          operation <- parseOperation
          (divisor, trueMonkeyId, falseMonkeyId) <- parseTest
          pure (monkeyId, Monkey{inspectionCount = 0, ..})

    monkeys <- parseMonkey `Attoparsec.sepBy` replicateM_ 2 Attoparsec.endOfLine

    Attoparsec.endOfLine

    pure $ Map.fromList monkeys


simulateRound :: (WorryLevel -> WorryLevel) -> Monkeys -> Monkeys
simulateRound extraSauce startingMonkeys =
  ST.runST $ do
    monkeysRef <- STRef.newSTRef startingMonkeys

    let monkeyIds =
          Map.keys startingMonkeys

    fmap (fromMaybe ()) . runMaybeT . for_ monkeyIds $ \monkeyId -> do
      monkey <- MaybeT $ Map.lookup monkeyId <$> STRef.readSTRef monkeysRef

      inspectionCountRef <- lift $ STRef.newSTRef 0

      for_ monkey.items $ \originalWorryLevel -> do
        lift $ STRef.modifySTRef' inspectionCountRef (+ 1)

        let adjustedWorryLevel =
              extraSauce $ monkey.operation originalWorryLevel

            targetMonkeyId
              | adjustedWorryLevel `mod` monkey.divisor == 0 = monkey.trueMonkeyId
              | otherwise = monkey.falseMonkeyId

        lift . STRef.modifySTRef' monkeysRef . flip Map.update targetMonkeyId $ \targetMonkey ->
          Just
            targetMonkey
              { items = targetMonkey.items |> adjustedWorryLevel
              }

      inspectionCount <- lift $ STRef.readSTRef inspectionCountRef

      lift . STRef.modifySTRef' monkeysRef $
        Map.insert
          monkeyId
          monkey
            { items = Seq.empty
            , inspectionCount = monkey.inspectionCount + inspectionCount
            }

    STRef.readSTRef monkeysRef


simulateRounds :: Int -> (WorryLevel -> WorryLevel) -> Monkeys -> Monkeys
simulateRounds numberOfRounds extraSauce startingMonkeys = do
  ST.runST $ do
    monkeysRef <- STRef.newSTRef startingMonkeys

    replicateM_ numberOfRounds $
      STRef.modifySTRef' monkeysRef (simulateRound extraSauce)

    STRef.readSTRef monkeysRef


solvePartOne :: Monkeys -> MonkeyBusinessLevel
solvePartOne monkeys =
  let extraSauce worryLevel = floor $ fromIntegral worryLevel / 3
      finalMonkeys = simulateRounds 20 extraSauce monkeys
      totalInspections = fmap (.inspectionCount) (Map.elems finalMonkeys)
   in MonkeyBusinessLevel . product . take 2 $ sortOn Down totalInspections


solvePartTwo :: Monkeys -> MonkeyBusinessLevel
solvePartTwo monkeys =
  let divisor = product $ fmap (.divisor) monkeys
      extraSauce = (`mod` divisor)
      finalMonkeys = simulateRounds 10_000 extraSauce monkeys
      totalInspections = fmap (.inspectionCount) (Map.elems finalMonkeys)
   in MonkeyBusinessLevel . product . take 2 $ sortOn Down totalInspections


solve :: Text -> Either ParseError (Answer MonkeyBusinessLevel MonkeyBusinessLevel)
solve text = do
  monkeys <- parseMonkeys text

  Right $
    Answer
      { partOne = solvePartOne monkeys
      , partTwo = solvePartTwo monkeys
      }
