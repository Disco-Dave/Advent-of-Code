module AdventOfCode.Parsers
  ( ParseError (..)
  , prettyPrintError
  , parse
  , natural
  , int
  , integer
  )
where

import Control.DeepSeq (NFData)
import Control.Exception (Exception)
import Data.Attoparsec.Text qualified as Attoparsec
import Data.Bifunctor (Bifunctor (first))
import Data.Scientific qualified as Scientific
import Data.Text (Text)
import Data.Text qualified as Text
import GHC.Natural (Natural)


newtype ParseError = ParseError
  { message :: Text
  }
  deriving (Show, Eq, NFData)


instance Exception ParseError


prettyPrintError :: ParseError -> Text
prettyPrintError ParseError{message} =
  "Parse error: " <> message


parse :: Attoparsec.Parser a -> Text -> Either ParseError a
parse parser input =
  first (ParseError . Text.pack) $
    Attoparsec.parseOnly
      ( parser
          <* Attoparsec.skipMany Attoparsec.endOfLine
          <* Attoparsec.endOfInput
      )
      input


natural :: Attoparsec.Parser Natural
natural = do
  scientific <- Attoparsec.scientific
  case Scientific.toBoundedInteger @Int scientific of
    Just parsedInt
      | parsedInt >= 0 -> pure $ fromIntegral parsedInt
    _ ->
      fail "Invalid number"


int :: Attoparsec.Parser Int
int = do
  scientific <- Attoparsec.scientific
  case Scientific.toBoundedInteger @Int scientific of
    Just parsedInt -> pure parsedInt
    _ -> fail "Invalid number"


integer :: Attoparsec.Parser Integer
integer = do
  scientific <- Attoparsec.scientific
  case Scientific.floatingOrInteger scientific of
    Left _ -> fail "Number wasn't an integer"
    Right i -> pure i
