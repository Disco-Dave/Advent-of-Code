module AdventOfCode.Cli
  ( ExecutionMode (..)
  , Args (..)
  , getArgs
  )
where

import AdventOfCode.Solutions qualified as Solutions
import Control.Applicative ((<|>))
import Data.Functor ((<&>))
import Data.List.NonEmpty (NonEmpty ((:|)))
import Data.Maybe (catMaybes)
import Options.Applicative qualified as Opt


data ExecutionMode
  = Sequential
  | Parallel
  deriving (Show, Eq)


mode :: Opt.Parser ExecutionMode
mode =
  Opt.flag Sequential Parallel . mconcat $
    [ Opt.short 'p'
    , Opt.long "parallel"
    , Opt.help "Run the solutions in parallel"
    ]


dayFlag :: Solutions.Day -> Opt.Parser (Maybe Solutions.Day)
dayFlag day =
  let number = fromEnum day + 1

      normal =
        Opt.flag Nothing (Just day) . mconcat $
          [ Opt.long ("day" <> show number)
          , Opt.help ("Solve the puzzle for day " <> show number)
          ]

      padded =
        Opt.flag Nothing (Just day) . mconcat $
          [ Opt.long ("day0" <> show number)
          , Opt.hidden
          , Opt.internal
          ]
   in if number < 10
        then padded <|> normal
        else normal


days :: Opt.Parser (NonEmpty Solutions.Day)
days =
  let allDays = traverse dayFlag [minBound ..]
   in allDays <&> \maybeDays ->
        case catMaybes maybeDays of
          (d : ds) -> d :| ds
          _ -> Solutions.Day01 :| [Solutions.Day02 ..]


data Args = Args
  { mode :: ExecutionMode
  , days :: NonEmpty Solutions.Day
  }
  deriving (Show, Eq)


args :: Opt.Parser Args
args =
  Args
    <$> mode
    <*> days


getArgs :: IO Args
getArgs =
  let mods =
        mconcat
          [ Opt.fullDesc
          , Opt.progDesc "Solve puzzles for Advent of Code (https://adventofcode.com/). If no days are specified, then all 25 days are ran."
          ]
   in Opt.execParser (Opt.info (Opt.helper <*> args) mods)
